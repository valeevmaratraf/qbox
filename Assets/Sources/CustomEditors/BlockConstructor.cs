﻿using System;
using System.Reflection;

using UnityEditor;

namespace qBox.EditorBehaviours.CustomEditors
{
    public enum BlockConstructorPreferenceType { MenuSection, OpenedFace }

    [CustomEditor(typeof(BlockEB))]
    public class BlockConstructor : Editor
    {
        private BlockEB blockEB;
        private int menuNumb;
        private int id;
        private int openedFaceNumber;

        private bool inEditMode
        {
            get { return !EditorApplication.isPlaying && UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage() == null; }
        }

        private static string[] menuSections = new string[]
        {
            "Global settings",
            "Faces",
            "Main"
        };

        public void OnEnable()
        {
            blockEB = target as BlockEB;

            SubscribeToDestroyingObject();

            id = GetIdFromInstance();

            menuNumb = GetOpenedMenuSectionNumb();
        }

        private int GetOpenedMenuSectionNumb()
        {
            string path = GetRegPath(BlockConstructorPreferenceType.MenuSection);
            if (inEditMode && EditorPrefs.HasKey(path))
                return EditorPrefs.GetInt(path);
            return 0;
        }

        private void SubscribeToDestroyingObject()
        {
            if (!blockEB.CheckDestroyedHandler(DeletePreferencesInReg))
                blockEB.DeletedFromScene += DeletePreferencesInReg;
        }

        public override void OnInspectorGUI()
        {
            menuNumb = EditorGUILayout.Popup(menuNumb, menuSections);

            string menuSectionRegPath = GetRegPath(BlockConstructorPreferenceType.MenuSection);
            if (inEditMode)
                EditorPrefs.SetInt(menuSectionRegPath, menuNumb);

            EditorGUILayout.Space(10);
            EditorGUI.indentLevel = 1;

            switch (menuNumb)
            {
                //Global settings
                case 0:
                    break;

                //Faces
                case 1:
                    string openedFaceRegPath = GetRegPath(BlockConstructorPreferenceType.OpenedFace);
                    openedFaceNumber = EditorPrefs.GetInt(openedFaceRegPath);

                    string[] directions = Enum.GetNames(typeof(Direction));
                    openedFaceNumber = EditorGUILayout.Popup(openedFaceNumber, directions);

                    if (inEditMode)
                        EditorPrefs.SetInt(openedFaceRegPath, openedFaceNumber);

                    var openedFaceEB = blockEB.GetFaceEditorBehaviour((Direction)openedFaceNumber);
                    var faceConstructor = CreateEditor(openedFaceEB);
                    faceConstructor.OnInspectorGUI();
                    break;

                case 2:
                    blockEB.Size = EditorGUILayout.Vector3IntField("Size", blockEB.Size);
                    break;
            }
        }

        private int GetIdFromInstance()
        {
            PropertyInfo inspectorModeInfo =
                typeof(SerializedObject).GetProperty("inspectorMode", BindingFlags.NonPublic | BindingFlags.Instance);

            SerializedObject serializedObject = new SerializedObject(blockEB.gameObject);
            inspectorModeInfo.SetValue(serializedObject, InspectorMode.Debug, null);

            SerializedProperty localIdProp =
                serializedObject.FindProperty("m_LocalIdentfierInFile");

            return localIdProp.intValue;
        }

        //TODO: Удалять ключ, когда удалили сцену или префаб (сделать тул какой-нибудь)
        private void DeletePreferencesInReg(BlockEB blockEB)
        {
            EditorPrefs.DeleteKey(GetRegPath(BlockConstructorPreferenceType.MenuSection));
            EditorPrefs.DeleteKey(GetRegPath(BlockConstructorPreferenceType.OpenedFace));
        }

        private string GetRegPath(BlockConstructorPreferenceType preferenceType)
        {
            string sceneName = blockEB.gameObject.scene.name;
            return string.Format("qBox/{0}/{1} {2}/{3}", sceneName, target.name, id, preferenceType.ToString());
        }
    }
}