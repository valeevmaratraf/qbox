﻿using UnityEditor;

namespace qBox.EditorBehaviours.CustomEditors
{
    [CustomEditor(typeof(FaceEB))]
    public class FaceConstructor : Editor
    {
        FaceEB faceEB;

        private void OnEnable()
        {
            faceEB = target as FaceEB;
        }

        public override void OnInspectorGUI()
        {
            EditorGUI.indentLevel = 1;
        }
    }
}