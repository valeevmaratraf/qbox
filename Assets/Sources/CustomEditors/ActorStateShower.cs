﻿using qBox.GameBehaviours.Actors;

using UnityEditor;

using UnityEngine;

namespace qBox.EditorBehaviours.CustomEditors
{
    [CustomEditor(typeof(ActorExtractor))]
    public class ActorStateShower : Editor
    {
        private IActor Actor => (target as ActorExtractor).Target;

        public override void OnInspectorGUI()
        {
            if (PrefabUtility.IsPartOfPrefabAsset(target))
            {
                EditorGUILayout.LabelField("Cant show state for prefab...");
                return;
            }

            if (!Application.isPlaying)
            {
                EditorGUILayout.LabelField("Cant show state in editor mode...");
                return;
            }

            GUI.enabled = false;

            EditorGUILayout.Vector3Field("Position", Actor.CenterPosition);
            EditorGUILayout.Vector3Field("Cell", Actor.GlobalStaticPosition);
            EditorGUILayout.EnumPopup("State", Actor.CurrentState);
            EditorGUILayout.IntField("Update priority", (int)Actor.UpdatePriority);
            EditorGUILayout.Toggle("On ground", Actor.IsOnGround);
            EditorGUILayout.EnumPopup("Gravity", Actor.GravityDirection);

            if (Actor is Q)
            {
                EditorGUILayout.Toggle("Is respawned", (Actor as Q).DebugRespawnded);
            }

            GUI.enabled = true;
        }
    }
}