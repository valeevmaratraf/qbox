﻿using UnityEditor;
using UnityEngine;

namespace qBox.EditorBehaviours.CustomEditors
{
    [CustomEditor(typeof(LevelEB))]
    public class LevelConstructor : Editor
    {
        private LevelEB levelEB;

        private void OnEnable()
        {
            levelEB = target as LevelEB;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            bool createShape = GUILayout.Button("Create tracery");
            if (createShape)
                levelEB.CreateTracery();
        }
    }
}