﻿#if UNITY_EDITOR
using qBox.GameBehaviours.World;
using System;
using UnityEditor;
using UnityEngine;

namespace qBox.EditorBehaviours
{
    [ExecuteInEditMode, RequireComponent(typeof(Block)), SelectionBase]
    public class BlockEB : MonoBehaviour
    {
        private SerializedObject blockSOCached;
        private Block blockCached;

        private SerializedObject blockSO
        {
            get
            {
                if (blockSOCached == null)
                    blockSOCached = new SerializedObject(block);
                return blockSOCached;
            }
        }
        private Block block
        {
            get
            {
                if (!blockCached)
                    blockCached = GetComponent<Block>();
                return blockCached;
            }
        }
        private FaceEB[] FacesEBs { get => GetComponentsInChildren<FaceEB>(); }

        public Vector3Int Size
        {
            get
            {
                return blockSO.FindProperty("size").vector3IntValue;
            }
            set
            {
                if (value != Size)
                {
                    value.Clamp(Vector3Int.zero, new Vector3Int(int.MaxValue, int.MaxValue, int.MaxValue));
                    blockSO.FindProperty("size").vector3IntValue = value;
                    blockSO.ApplyModifiedProperties();

                    SetFacesSize(value);
                }
            }
        }

        public event Action<BlockEB> DeletedFromScene;

        public bool CheckDestroyedHandler(Action<BlockEB> handler)
        {
            if (DeletedFromScene == null)
                return false;

            foreach (Delegate d in DeletedFromScene.GetInvocationList())
                if (d.Method == handler.Method)
                    return true;

            return false;
        }

        public FaceEB GetFaceEditorBehaviour(Direction normal)
        {
            foreach (FaceEB face in FacesEBs)
                if (face.Normal == normal)
                    return face;
            return null;
        }

        //public ShapesCollection CreateTracery(string rootFolder)
        //{
        //	string path = rootFolder + "/" + gameObject.name;
        //	if (!AssetDatabase.IsValidFolder(path))
        //		AssetDatabase.CreateFolder(rootFolder, gameObject.name);

        //	ShapesCollection blockShapes = ScriptableObject.CreateInstance<ShapesCollection>();
        //	blockShapes.name = gameObject.name;

        //	foreach (FaceEB faceEB in FacesEBs)
        //		blockShapes.AddSubCollection(faceEB.CreateTracery(path));

        //	AssetDatabase.CreateAsset(blockShapes, path + string.Format("/{0}.asset", gameObject.name));

        //	return blockShapes;
        //}

        private void SetFacesSize(Vector3Int value)
        {
            foreach (FaceEB faceEB in FacesEBs)
            {
                switch (faceEB.Normal)
                {
                    case Direction.Left:
                    case Direction.Right:
                        faceEB.SetSize(value.z, value.y, value.x);
                        break;

                    case Direction.Up:
                    case Direction.Down:
                        faceEB.SetSize(value.x, value.z, value.y);
                        break;

                    case Direction.Forward:
                    case Direction.Back:
                        faceEB.SetSize(value.x, value.y, value.z);
                        break;
                }
            }
        }

        private void OnDestroy()
        {
            if (!EditorApplication.isPlayingOrWillChangePlaymode)
                if (Time.frameCount != 0 && Time.renderedFrameCount != 0)
                    DeletedFromScene?.Invoke(this);
        }
    }
}
#endif