﻿using SvgNet.SvgGdi;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Unity.VectorGraphics;
using UnityEditor;
using UnityEngine;

namespace qBox.EditorBehaviours
{
    public enum CommonShapeType
    {
        HallowSquare, SolidSquare, HallowCircle, SolidCircle
    }
    public enum TraceryLineType
    {
        Vertical, Horizontal, CornerUpRight, CornerUpLeft
    }
    public enum BordersLineType
    {
        Up, Right, Down, Left, UpRight, DownRight, DownLeft, UpLeft
    }

    public class SVGShapeCreatorWizard : ScriptableWizard
    {
        [SerializeField] private float conturWidth = 1;
        [SerializeField] private string savePath = "Assets/Trash";
        [SerializeField] private int scaleFactor = 1;

        private Pen pen;
        private SvgGraphics svgGraphics;

        private string commonShapesFoldername = "Common shapes";


        [MenuItem("Tracery tools/Common shapes creator")]
        private static void CreateWizard()
        {
            DisplayWizard<SVGShapeCreatorWizard>("Common shapes creator wizard");
        }

        private void OnWizardCreate()
        {
            CreateCommonShapes();
        }

        private void CreateCommonShapes()
        {
            Initialize();
            List<ShapeSaveInfo> shapes = new List<ShapeSaveInfo>();
            if (!AssetDatabase.IsValidFolder(savePath + commonShapesFoldername))
                AssetDatabase.CreateFolder(savePath, commonShapesFoldername);

            for (int i = 0; i < Enum.GetValues(typeof(CommonShapeType)).Length; i++)
            {
                if (i != 2)
                    continue;

                shapes.Add(new ShapeSaveInfo()
                {
                    Shape = CreateShape((CommonShapeType)i),
                    LocalPath = commonShapesFoldername + "/" + ((CommonShapeType)i).ToString()
                });
            }

            foreach (var s in shapes)
                SaveShape(s);
        }

        private void Initialize()
        {
            pen = new Pen(System.Drawing.Color.White, conturWidth);
            svgGraphics = new SvgGraphics();
        }

        private Shape CreateShape(CommonShapeType type)
        {
            svgGraphics.Clear(System.Drawing.Color.White);

            switch (type)
            {
                case CommonShapeType.HallowCircle:
                    svgGraphics.DrawEllipse(pen, 0, 0, 1 * scaleFactor, 1 * scaleFactor);
                    break;
            }
            return null;
        }

        private Shape CreateTraceryLine(TraceryLineType type)
        {
            return null;
        }

        private Shape CreateBorderLine(BordersLineType type)
        {
            return null;
        }

        private void SaveShape(ShapeSaveInfo s)
        {
            if (!AssetDatabase.IsValidFolder(savePath))
                AssetDatabase.CreateFolder(savePath, s.LocalPath);

            string svg = svgGraphics.WriteSVGString();
            string path = string.Format(savePath + "/" + s.LocalPath + ".svg");
            using (StreamWriter sw = new StreamWriter(path))
                sw.Write(svg);
            AssetDatabase.Refresh();
        }

        private void OnValidate()
        {

        }

        private struct ShapeSaveInfo
        {
            public Shape Shape { get; set; }
            public string LocalPath { get; set; }
        }
    }
}