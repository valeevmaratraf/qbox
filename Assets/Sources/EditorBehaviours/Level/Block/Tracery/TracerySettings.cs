﻿using UnityEngine;

namespace qBox.EditorBehaviours.Tracery
{
    [CreateAssetMenu(fileName = "Tracery settings", menuName = "qBox/Tracery/Tracery settings")]
    public class TracerySettings : ScriptableObject
    {
        [SerializeField] private int cellsPerUnit = 8;

        public int CellsPerUnit => cellsPerUnit;
    }
}