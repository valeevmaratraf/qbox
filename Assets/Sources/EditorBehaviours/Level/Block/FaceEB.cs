﻿using qBox.EditorBehaviours.Tracery;
using qBox.GameBehaviours.World;
using System.Collections.Generic;
using UnityEngine;

namespace qBox.EditorBehaviours
{
    //TODO: Сделать уничтожение лишних фрагментов, если они перекрыты и не видны
    //TODO: Сделать не удаление всех фрагментов и создание новых,
    //а сохранение предыдущих при переразмерении и удаление только лишних (создание новых)
    [ExecuteInEditMode, RequireComponent(typeof(Face))]
    public class FaceEB : MonoBehaviour
    {
        //TODO: перенести size в game behaviour
        [SerializeField] private Vector2Int size;
        [SerializeField] private GameObject faceFragmentPrefab;
        [SerializeField] private GameObject fragmetsRoot;
#if UNITY_EDITOR

        private Face faceCached;

        private Face face
        {
            get
            {
                if (!faceCached)
                    faceCached = GetComponent<Face>();
                return faceCached;
            }
        }

        public Direction Normal { get => face.Normal; }

        private void Update()
        {
            switch (Normal)
            {
                case Direction.Left:
                    transform.rotation = Quaternion.LookRotation(Vector3.left, Vector3.up);
                    break;

                case Direction.Right:
                    transform.rotation = Quaternion.LookRotation(Vector3.right, Vector3.up);
                    break;

                case Direction.Up:
                    transform.rotation = Quaternion.LookRotation(Vector3.up, Vector3.forward);
                    break;

                case Direction.Down:
                    transform.rotation = Quaternion.LookRotation(Vector3.down, Vector3.forward);
                    break;

                case Direction.Forward:
                    transform.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);
                    break;

                case Direction.Back:
                    transform.rotation = Quaternion.LookRotation(Vector3.back, Vector3.up);
                    break;
            }
        }

        private void DestroyFragments()
        {
            List<Transform> fragmentTransforms = new List<Transform>();
            fragmetsRoot.GetComponentsInChildren(fragmentTransforms);
            fragmentTransforms.Remove(fragmetsRoot.transform);

            int fragsCount = fragmentTransforms.Count;
            for (int i = fragsCount - 1; i >= 0; i--)
                DestroyImmediate(fragmentTransforms[i].gameObject);
        }

        public void SetSize(int width, int height, int indent)
        {
            DestroyFragments();

            size = new Vector2Int(width, height);

            switch (Normal)
            {
                case Direction.Left:
                case Direction.Down:
                    transform.localPosition = new Vector3(0, 0, 0);
                    break;

                case Direction.Right:
                    transform.localPosition = new Vector3(indent, 0, size.x);
                    break;

                case Direction.Up:
                    transform.localPosition = new Vector3(size.x, indent, 0);
                    break;

                case Direction.Forward:
                    transform.localPosition = new Vector3(0, 0, indent);
                    break;

                case Direction.Back:
                    transform.localPosition = new Vector3(size.x, 0, 0);
                    break;
            }

            for (int x = 0; x < width; x++)
                for (int y = 0; y < height; y++)
                {
                    var faceFrag = Instantiate(faceFragmentPrefab, fragmetsRoot.transform);
                    faceFrag.name = string.Format("{0}, {1}", x, y);
                    faceFrag.isStatic = true;
                    faceFrag.transform.localPosition = new Vector3(0.5F + x, 0.5F + y);
                }
        }

        public void CreateTracery(TracerySettings settings)
        {
            for (int x = 0; x < size.x; x++)
                for (int y = 0; y < size.y; y++)
                {

                }
            //	string path = rootFolder + "/" + Normal;
            //	if (!AssetDatabase.IsValidFolder(path))
            //		AssetDatabase.CreateFolder(rootFolder, Normal.ToString());

            //	var faceShapes = ScriptableObject.CreateInstance<ShapesCollection>();
            //	faceShapes.name = Normal.ToString();



            //	AssetDatabase.CreateAsset(faceShapes, path + string.Format("/{0}.asset", Normal));
            //	return faceShapes;
        }
#endif
    }
}
