﻿using UnityEngine;

namespace qBox.EditorBehaviours
{
    public class LevelEB : MonoBehaviour
    {
        [SerializeField] private bool drawBounds = false;
        //[SerializeField] private ShapesCollection levelShapes;

        private BlockEB[] BlockEBs => GetComponentsInChildren<BlockEB>();

#if UNITY_EDITOR
        public void OnDrawGizmosSelected()
        {
            if (!drawBounds)
                return;

            var blocks = BlockEBs;

            foreach (var block in blocks)
            {
                Gizmos.DrawCube(block.transform.position + (Vector3)block.Size / 2, block.Size);
            }
        }

        public void CreateTracery()
        {

            //	ShapesCollection.ValidatePaths();

            //	var sceneName = SceneManager.GetActiveScene().name;
            //	string path = ShapesCollection.ShapesPath + string.Format("/{0}", sceneName);
            //	if (!AssetDatabase.IsValidFolder(path))
            //		AssetDatabase.CreateFolder(ShapesCollection.ShapesPath, sceneName);

            //	levelShapes = ScriptableObject.CreateInstance<ShapesCollection>();
            //	levelShapes.name = sceneName;

            //	foreach (BlockEB blockEB in BlockEBs)
            //		levelShapes.AddSubCollection(blockEB.CreateTracery(path));

            //	AssetDatabase.CreateAsset(levelShapes, path + string.Format("/{0} shapes collection.asset", sceneName));
        }
#endif
    }
}