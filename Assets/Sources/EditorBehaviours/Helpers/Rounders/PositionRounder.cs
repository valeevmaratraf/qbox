﻿using UnityEngine;

namespace qBox.EditorBehaviours.Controllers
{
    [ExecuteAlways, DisallowMultipleComponent]
    public class PositionRounder : MonoBehaviour
    {
        [SerializeField] private Vector3 zeroOffset = Vector3.zero;
        [SerializeField] private Vector3 cellSize = Vector3.one;
        [SerializeField] private bool autoExecute = false;
        [SerializeField] private Space mode = Space.Self;
#if UNITY_EDITOR
        private void Update()
        {
            if (!Application.isPlaying && autoExecute && transform.hasChanged)
                RoundPosition();
        }

        public void RoundPosition()
        {
            if (mode == Space.Self)
                transform.localPosition = transform.localPosition.RoundTo(cellSize, zeroOffset);
            else if (mode == Space.World)
                transform.position = transform.position.RoundTo(cellSize, zeroOffset);
        }
#endif
    }
}