﻿using qBox.GameBehaviours.Actors;
using UnityEngine;

namespace qBox.EditorBehaviours
{
    [ExecuteInEditMode]
    public class ActorExtractor : MonoBehaviour
    {
#if UNITY_EDITOR
        public IActor Target => GetComponent<IActor>();
#endif
    }
}
