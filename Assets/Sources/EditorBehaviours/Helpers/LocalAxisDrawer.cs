using UnityEngine;

namespace qBox.EditorBehaviours
{
    [ExecuteInEditMode, DisallowMultipleComponent]
    public class LocalAxisDrawer : MonoBehaviour
    {
        [SerializeField] private bool onSelected = true;
#if UNITY_EDITOR
        private Transform target;

        private void OnDrawGizmosSelected()
        {
            if (onSelected)
                DrawAxises();
        }

        private void OnDrawGizmos()
        {
            if (!onSelected)
                DrawAxises();
        }

        private void DrawAxises()
        {
            if (!target)
                target = transform;

            Gizmos.color = Color.green;
            Gizmos.DrawRay(target.position, target.up);

            Gizmos.color = Color.red;
            Gizmos.DrawRay(target.position, target.right);

            Gizmos.color = Color.blue;
            Gizmos.DrawRay(target.position, target.forward);
        }
#endif
    }
}
