﻿using UnityEngine;

namespace qBox.EditorBehaviours
{
    public class BoxDrawer : MonoBehaviour
    {
        [SerializeField] Color color = Color.green;
#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.color = color;
            Gizmos.DrawCube(transform.position, Vector3.one);
        }
#endif
    }
}