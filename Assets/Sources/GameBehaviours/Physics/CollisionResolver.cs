﻿using UnityEngine;

namespace qBox.GameBehaviours
{
    public static class CollisionResolver
    {
        //TODO: запихать в настройки
        public const float Epsilon = 0.001F;

        public static CollisionResovleResult CalcCollision(CollisionResolverCalcInfo info)
        {
            var obj1 = info.Object1;
            var obj2 = info.Object2;

            bool collised = Physics.ComputePenetration(obj1.Collider, obj1.CenterPosition, obj1.Rotation,
                                       obj2.Collider, obj2.CenterPosition, obj2.Rotation,
                                       out Vector3 _, out float resolveDistance);

            if (!collised || resolveDistance < Epsilon)
            {
                return null;
            }

            return new CollisionResovleResult() { Distance = resolveDistance };
        }

        public static bool ResolveCollision(ResolveInfo info)
        {
            bool wasCollision = false;

            var soft = info.SoftActor;
            var hard = info.HardActor;

            int iterBreacker = 0;
            bool collised = true;
            while (collised)
            {
                collised = Physics.ComputePenetration(soft.Collider, soft.CenterPosition, soft.Rotation,
                                                      hard.Collider, hard.CenterPosition, hard.Rotation,
                                                      out _, out float resolveDistance);
                if (collised && resolveDistance > Epsilon)
                {
                    wasCollision = true;
                    Axis constraint = info.Constraint;
                    Vector3 resolveDirection = default;
                    switch (constraint)
                    {
                        case Axis.X: resolveDirection = hard.CenterPosition.x < soft.CenterPosition.x ? Vector3.right : Vector3.left; break;
                        case Axis.Y: resolveDirection = hard.CenterPosition.y < soft.CenterPosition.y ? Vector3.up : Vector3.down; break;
                        case Axis.Z: resolveDirection = hard.CenterPosition.z < soft.CenterPosition.z ? Vector3.forward : Vector3.back; break;

                    }
                    soft.CenterPosition += resolveDirection * resolveDistance;
                }
                else
                {
                    break;
                }

                iterBreacker++;
                if (iterBreacker > 1000)
                {
                    Debug.LogError("Resolve collision failed!");
                    break;
                }
            }

            return wasCollision;
        }
    }
}