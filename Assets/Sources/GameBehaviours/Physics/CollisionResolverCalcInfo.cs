﻿using qBox.GameBehaviours.Actors;

namespace qBox.GameBehaviours
{
    public class CollisionResolverCalcInfo
    {
        public IActor Object1 { get; set; }
        public IActor Object2 { get; set; }

        public CollisionResolverCalcInfo(IActor object1, IActor object2)
        {
            Object1 = object1;
            Object2 = object2;
        }
    }
}