﻿using qBox.GameBehaviours.Actors;

namespace qBox.GameBehaviours
{
    public class ResolveInfo
    {
        public IActor HardActor { get; set; }
        public IActor SoftActor { get; set; }
        public Axis Constraint { get; set; }

        public ResolveInfo(IActor hardActor, IActor softActor, Axis constraint)
        {
            HardActor = hardActor;
            SoftActor = softActor;
            Constraint = constraint;
        }
    }
}