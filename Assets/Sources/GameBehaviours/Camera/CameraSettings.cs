﻿using UnityEngine;

namespace qBox.GameBehaviours.GameCamera
{
    [CreateAssetMenu(fileName = "CameraSettings", menuName = "qBox/Settings/Camera")]
    public class CameraSettings : ScriptableObject
    {
        #region Serialized fields
        #region Rotation
        [SerializeField] private float cameraRotationSpeed = 480;
        [SerializeField] private float angleDeceleration = 0.05F;
        [SerializeField] private float maxRotationSpeed = 180;
        [SerializeField] private float deathRotationRange = 15;
        #endregion

        #region Scroll
        [SerializeField] private float initialDistance = 15;
        [SerializeField] private float scrollSpeed = 10;
        [SerializeField] private float maxDistance = 50;
        [SerializeField] private float minDistance = 5;
        [SerializeField] private float scrollDamping = 0.1F;
        #endregion

        #region Input
        [SerializeField] private KeyCode clockwiseRotationKey = KeyCode.Q;
        [SerializeField] private KeyCode antiClockwiseRotationKey = KeyCode.E;
        [SerializeField] private string scrollAxisName = "Mouse ScrollWheel";
        #endregion
        #endregion

        #region Public properties
        #region Rotation
        public float CameraRotationSpeed
        {
            get { return cameraRotationSpeed; }
        }
        public float AngleDeceleration
        {
            get { return angleDeceleration; }
        }
        public float MaxRotationSpeed
        {
            get { return maxRotationSpeed; }
        }
        public float DeathRotationRange
        {
            get { return deathRotationRange; }
        }
        #endregion

        #region Scroll
        public float InitialDistance
        {
            get { return initialDistance; }
        }
        public float MaxDistance
        {
            get { return maxDistance; }
        }
        public float MinDistance
        {
            get { return minDistance; }
        }
        public float ScrollSpeed
        {
            get { return scrollSpeed; }
        }
        public float ScrollDamping
        {
            get { return scrollDamping; }
        }
        #endregion

        #region Input
        public KeyCode ClockwiseRotationKey
        {
            get { return clockwiseRotationKey; }
        }
        public KeyCode AntiClockwiseRotationKey
        {
            get { return antiClockwiseRotationKey; }
        }
        public string ScrollAxisName
        {
            get { return scrollAxisName; }
        }
        #endregion
        #endregion

        private void OnValidate()
        {
            cameraRotationSpeed = Mathf.Clamp(cameraRotationSpeed, 0, float.MaxValue);
            angleDeceleration = Mathf.Clamp01(angleDeceleration);
            maxRotationSpeed = Mathf.Clamp(maxRotationSpeed, 0, float.MaxValue);
            deathRotationRange = Mathf.Clamp(deathRotationRange, 0, 360);

            scrollSpeed = Mathf.Clamp(scrollSpeed, 0, float.MaxValue);
            minDistance = Mathf.Clamp(minDistance, 0, float.MaxValue);
            maxDistance = Mathf.Clamp(maxDistance, minDistance, float.MaxValue);
            initialDistance = Mathf.Clamp(initialDistance, minDistance, maxDistance);
            scrollDamping = Mathf.Clamp01(scrollDamping);
        }
    }
}