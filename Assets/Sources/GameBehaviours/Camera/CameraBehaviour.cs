﻿using qBox.GameBehaviours.Actors;
using UnityEngine;
using Zenject;

namespace qBox.GameBehaviours.GameCamera
{
    //TODO: написать CameraEditorBehaviour
    //TODO: написать и интегрировать InputManager
    [DisallowMultipleComponent]
    public class CameraBehaviour : MonoBehaviour, ISimpleUpdatable
    {
        private CameraSettings settings;
        private Transform focus;
        private Q_Orientation orientation;
        private Transform cameraT;

        private float deltaAngle;
        private float currentDistance;

        public uint UpdatePriority { get; set; } = UpdatePriorityList.Camera;
        public bool Enabled => isActiveAndEnabled;
        public SimpleUpdatableOrder Order => SimpleUpdatableOrder.AfterStepByStep;

        [Inject]
        private void Construct(ConstructArgs args)
        {
            settings = args.Settings;
            focus = args.Focus;
            orientation = args.Orientation;

            cameraT = transform;

            currentDistance = settings.InitialDistance;
            cameraT.position = currentDistance * -cameraT.forward + focus.position;
        }

        public TickResult Tick(float simTime, bool _)
        {
            SetPosition();

            bool rotating = Rotate(simTime);

            Pose orientatedFocus = new Pose(focus.position, orientation.CurrentPose.rotation);
            deltaAngle = Mathf.Clamp(deltaAngle, -settings.MaxRotationSpeed, settings.MaxRotationSpeed);
            cameraT.RotateAround(orientatedFocus.position, orientatedFocus.up, deltaAngle * simTime);

            if (!rotating)
            {
                Decelerate();
            }

            return new TickResult(simTime);
        }

        private bool Rotate(float simTime)
        {
            if (Input.GetKey(settings.AntiClockwiseRotationKey))
            {
                deltaAngle -= settings.CameraRotationSpeed * simTime;
                return true;
            }
            if (Input.GetKey(settings.ClockwiseRotationKey))
            {
                deltaAngle += settings.CameraRotationSpeed * simTime;
                return true;
            }
            return false;
        }
        private void Decelerate()
        {
            deltaAngle = Mathf.Lerp(deltaAngle, 0, settings.AngleDeceleration);

            if (Mathf.Abs(deltaAngle) <= settings.DeathRotationRange)
            {
                deltaAngle = 0;
            }
        }
        private void SetPosition()
        {
            currentDistance -= Input.GetAxis(settings.ScrollAxisName) * settings.ScrollSpeed;
            currentDistance = Mathf.Clamp(currentDistance, settings.MinDistance, settings.MaxDistance);
            Vector3 desiredPos = currentDistance * -cameraT.forward + focus.position;
            cameraT.position = Vector3.Lerp(cameraT.position, desiredPos, settings.ScrollDamping);
        }

        public class ConstructArgs
        {
            public CameraSettings Settings { get; set; }
            public Transform Focus { get; set; }
            public Q_Orientation Orientation { get; set; }
        }
    }
}