﻿using UnityEngine;

using Zenject;

namespace qBox.GameBehaviours.GameCamera
{
    [DisallowMultipleComponent]
    public class CameraFocus : MonoBehaviour, ISimpleUpdatable
    {
        private ICameraFocusTarget moveTarget;
        private Transform focusT;
        private Camera mainCamera;
        private Transform parent;
        //TODO: Впихать в scriptableObject
        [SerializeField] private float deathDistance = 5F;

        public uint UpdatePriority { get; set; } = UpdatePriorityList.CameraFocus;
        public bool Enabled => isActiveAndEnabled;
        public SimpleUpdatableOrder Order => SimpleUpdatableOrder.AfterStepByStep;

        [Inject]
        private void Construct(ConstructArgs args)
        {
            moveTarget = args.MoveTarget;
            mainCamera = args.MainCamera;

            moveTarget.OnTeleportStart += OnTeleportStartHandler;
            moveTarget.OnTeleportEnd += OnTeleportEndHandler;

            focusT = transform;
            parent = focusT.parent;
        }

        public TickResult Tick(float simTime, bool _)
        {
            Vector2 screenCenter = new Vector2(Screen.width / 2, Screen.height / 2);
            Vector2 targetScreenPos = mainCamera.WorldToScreenPoint(moveTarget.Position);

            float distance = Vector2.Distance(targetScreenPos, screenCenter);
            if (distance > deathDistance)
            {
                float maxDistance = Screen.width > Screen.height ? Screen.height : Screen.width;
                focusT.position = Vector3.Lerp(focusT.position, moveTarget.Position, distance / maxDistance);
            }
            return new TickResult(simTime);
        }

        private void OnTeleportStartHandler(Transform target)
        {
            focusT.SetParent(target);
        }

        private void OnTeleportEndHandler(Transform target)
        {
            focusT.SetParent(parent);
        }

        public class ConstructArgs
        {
            public ICameraFocusTarget MoveTarget { get; set; }
            public Camera MainCamera { get; set; }
        }
    }
}