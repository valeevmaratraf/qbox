﻿using System;

using UnityEngine;

namespace qBox.GameBehaviours.GameCamera
{
    public interface ICameraFocusTarget
    {
        event Action<Transform> OnTeleportStart;
        event Action<Transform> OnTeleportEnd;

        Vector3 Position { get; }
    }
}