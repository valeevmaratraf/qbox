﻿using qBox.GameBehaviours.Actors;
using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace qBox.GameBehaviours.World
{
    public class Level : MonoBehaviour, IEnvironment
    {
        private List<IQBoxObject> objects;
        private VisabilityRegister staticObjectsVisabilityRegister;

        public bool IsVisibleOnScreen => staticObjectsVisabilityRegister.IsVisible;

        [Inject]
        private void Construct(UpdateManager updateManager)
        {
            objects = new List<IQBoxObject>(64);
            staticObjectsVisabilityRegister = new VisabilityRegister();
            updateManager.RegisterUpdatable(staticObjectsVisabilityRegister);
        }

        public void RegisterObjectAsLevelPart(IQBoxObject target)
        {
            TryRegisterVisibleStaticLevelPart(target);
            objects.Add(target);

            target.OnDestroyEvent += DestroyObjectHandler;
            target.OnEnableChange += ObjectEnableChangeHandler;
        }

        public IActor[] GetActorsAtPosition(Vector3Int position, Predicate<IActor> finder = null, IActor exclude = null)
        {
            List<IActor> findedObjects = new List<IActor>();

            foreach (IQBoxObject obj in objects)
            {
                //TODO: отрфечить, выполнить удаление из списка объектов те, которые равны null
                if (!(obj is IActor))
                {
                    continue;
                }

                var actor = obj as IActor;

                foreach (var cell in actor.TakedCells)
                {
                    if (cell == position && exclude != actor)
                    {
                        if (finder != null && !finder(actor))
                        {
                            continue;
                        }
                        findedObjects.Add(actor);
                    }
                }
            }

            return findedObjects.ToArray();
        }
        public bool IsCellWalkable(Vector3Int globalPosition)
        {
            foreach (IQBoxObject obj in objects)
            {
                if (obj is ISolid && !(obj is IActor))
                {
                    var solidVolumedObj = obj as ISolid;
                    BoundsInt objBounds = new BoundsInt(solidVolumedObj.GlobalStaticPosition,
                                                        solidVolumedObj.Size);

                    if (objBounds.Contains(globalPosition))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public IActor[] GetNearActors(Vector3Int cell)
        {
            List<IActor> nears = new List<IActor>();
            foreach (Vector3Int nearPos in cell.GetNearPositions())
            {
                var nearActors = GetActorsAtPosition(nearPos, null);
                foreach (var actor in nearActors)
                {
                    nears.Add(actor);
                }
            }
            return nears.ToArray();
        }

        //TODO: абстрагироваться от блока, сделать интерфейс IInteractive
        public void Interact(IActor target)
        {
            foreach (var obj in objects)
            {
                if (!(obj is Block))
                {
                    continue;
                }

                var block = obj as Block;
                block.CheckInteract(target);
            }
        }

        private void DestroyObjectHandler(IQBoxObject obj)
        {
            if (!Application.isPlaying)
            {
                return;
            }

            obj.OnEnableChange -= ObjectEnableChangeHandler;
            objects.Remove(obj);
            TryUnregisterVisibleStaticLevelPart(obj);
        }

        private void ObjectEnableChangeHandler(IQBoxObject obj, bool value)
        {
            if (!value)
            {
                objects.Remove(obj);
                TryUnregisterVisibleStaticLevelPart(obj);
            }
            else
            {
                objects.Add(obj);
                TryRegisterVisibleStaticLevelPart(obj);
            }
        }

        private void TryRegisterVisibleStaticLevelPart(IQBoxObject target)
        {
            if (target is IVisibleEnviromentStaticPart && !(target is IActor))
            {
                staticObjectsVisabilityRegister.AddRenderers((target as IVisibleEnviromentStaticPart).Parts);
            }
        }

        private void TryUnregisterVisibleStaticLevelPart(IQBoxObject target)
        {
            if (target is IVisibleEnviromentStaticPart && !(target is IActor))
            {
                staticObjectsVisabilityRegister.RemoveRenderers((target as IVisibleEnviromentStaticPart).Parts);
            }
        }
    }
}
