﻿using qBox.GameBehaviours.Actors;
using System;

using UnityEngine;

namespace qBox.GameBehaviours.World
{
    public interface IEnvironment
    {
        IActor[] GetActorsAtPosition(Vector3Int position, Predicate<IActor> finder = null, IActor exlude = null);
        IActor[] GetNearActors(Vector3Int cell);
        void Interact(IActor actor);

        bool IsCellWalkable(Vector3Int globalPosition);
        bool IsVisibleOnScreen { get; }

        void RegisterObjectAsLevelPart(IQBoxObject target);
    }
}