﻿using qBox.GameBehaviours.Actors;

namespace qBox.GameBehaviours.World
{
    public interface ISpecialFragment
    {
        void Interact(IActor actor);
    }
}