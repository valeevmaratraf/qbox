﻿using qBox.GameBehaviours.Actors;
using UnityEngine;

namespace qBox.GameBehaviours.World
{
    public class LevelTarget : MonoBehaviour, ISpecialFragment
    {
        private bool isTriggered = false;

        //TODO: абстрагироваться от Box, отрефачить
        public void Interact(IActor actor)
        {
            if (!(actor is Box))
                return;

            Vector3 above = transform.position + transform.forward / 2;
            Vector3Int adjacentCell = Vector3Int.FloorToInt(above);
            if (actor.GlobalStaticPosition == adjacentCell && !isTriggered)
            {
                isTriggered = true;
                Destroy((actor as Box).gameObject);
                GetComponentInChildren<Renderer>().material.color = Color.green;
            }
        }
    }
}
