﻿using qBox.GameBehaviours.Actors;
using UnityEngine;

namespace qBox.GameBehaviours.World
{
    //TODO: нужен рефакторинг
    public class GravityChanger : MonoBehaviour, ISpecialFragment
    {
        [SerializeField] private Direction gravity;

        public void Interact(IActor actor)
        {
            Vector3 above = transform.position + transform.forward / 2;
            Vector3Int adjacentCell = Vector3Int.FloorToInt(above);
            if (actor.GlobalStaticPosition == adjacentCell)
            {
                actor.GravityDirection = gravity;
            }
        }
    }
}