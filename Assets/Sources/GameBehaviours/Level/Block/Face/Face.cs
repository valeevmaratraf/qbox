﻿using qBox.GameBehaviours.Actors;
using UnityEngine;

namespace qBox.GameBehaviours.World
{
    //TODO: реализовать через инъекцию инициализацию
    public class Face : MonoBehaviour
    {
        [SerializeField, HideInInspector] private Direction normal;

        public Direction Normal { get => normal; }

        private ISpecialFragment[] specialFragments;

        private void Awake()
        {
            specialFragments = GetComponentsInChildren<ISpecialFragment>();
        }

        public void CheckInteract(IActor target)
        {
            foreach (var specFrag in specialFragments)
            {
                specFrag.Interact(target);
            }
        }
    }
}