using UnityEngine;
using Zenject;

namespace qBox.GameBehaviours.World
{
    public class BlockInstaller : MonoInstaller
    {
        [SerializeField] private BoxCollider blockCollider;

        public override void InstallBindings()
        {
            Face[] faces = transform.GetComponentsInChildren<Face>();

            Block.ConstructArgs args = new Block.ConstructArgs()
            {
                Faces = faces,
                Collider = blockCollider
            };

            Container.Bind<Block.ConstructArgs>().FromInstance(args).AsSingle();
        }
    }
}