﻿using qBox.GameBehaviours.Actors;
using System;
using System.Collections.Generic;

using UnityEngine;

using Zenject;

namespace qBox.GameBehaviours.World
{
    public class Block : MonoBehaviour, ISolid, IVisibleEnviromentStaticPart
    {
        [SerializeField, HideInInspector] private Vector3Int size;

        private BoxCollider blockCollider;
        private Face[] faces;
        private Transform blockT;

        public Vector3Int Size => size;
        public Collider Collider => blockCollider;
        public Vector3Int GlobalStaticPosition => Vector3Int.RoundToInt(blockT.position);
        public Vector3 CenterPosition
        {
            get => ((Vector3)size) / 2 + blockT.position;
            set => throw new NotImplementedException();
        }
        public Renderer[] Parts => GetComponentsInChildren<Renderer>();
        public Quaternion Rotation => blockT.rotation;
        public Vector3Int[] TakedCells
        {
            get
            {
                List<Vector3Int> cells = new List<Vector3Int>(Size.x * Size.y * Size.z);
                for (int w = 0; w < Size.x; w++)
                {
                    for (int l = 0; l < Size.z; l++)
                    {
                        for (int h = 0; h < Size.y; h++)
                        {
                            cells.Add(new Vector3Int(GlobalStaticPosition.x + w,
                                                     GlobalStaticPosition.y + h,
                                                     GlobalStaticPosition.z + l));
                        }
                    }
                }
                return cells.ToArray();
            }
        }
        public IEnvironment Environment { get; private set; }

        public event Action<IQBoxObject> OnDestroyEvent;
        public event Action<IQBoxObject, bool> OnEnableChange;

        [Inject]
        private void Construct(ConstructArgs args, IEnvironment environment)
        {
            blockCollider = args.Collider;
            faces = args.Faces;
            Environment = environment;

            blockT = transform;

            blockCollider.size = size;
            blockCollider.center = (Vector3)size / 2;
        }

        private void Start()
        {
            Environment.RegisterObjectAsLevelPart(this);
        }

        public void CheckInteract(IActor target)
        {
            foreach (var face in faces)
            {
                face.CheckInteract(target);
            }
        }

        private void OnDestroy()
        {
            OnDestroyEvent?.Invoke(this);
        }

        private void OnEnable()
        {
            OnEnableChange?.Invoke(this, true);
        }

        private void OnDisable()
        {
            OnEnableChange?.Invoke(this, false);
        }

        public class ConstructArgs
        {
            public Face[] Faces { get; set; }
            public BoxCollider Collider { get; set; }
        }
    }
}