using qBox.GameBehaviours.Actors;
using qBox.GameBehaviours.GameCamera;
using qBox.GameBehaviours.World;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace qBox.GameBehaviours
{
    public class SceneInstaller : MonoInstaller
    {
        //Settings assets
        [SerializeField] private GlobalGameSettings gameSettings;
        [SerializeField] private CameraSettings cameraSettings;
        [SerializeField] private Q_OrientationSettings orientationSettings;

        //Scene objects
        [SerializeField] private UpdateManager updateManager;
        [SerializeField] private Camera mainCamera;
        [SerializeField] private Q_Orientation QOrientation;
        [SerializeField] private CameraFocus cameraFocus;
        [SerializeField] private Level level;
        [SerializeField] private GameObject sceneRoot;

        //Prefabs
        [SerializeField] private GameObject QPrefab;
        [SerializeField] private GameObject BoxPrefab;

        public override void InstallBindings()
        {
            BindQOrientationDependencies();
            BindQDependencies();

            var actorSpawners = FindAllSpawners();

            SpawnQ(actorSpawners);
            SpawnBoxes(actorSpawners);

            BindCameraDependencies();
            BindCameraFocusDependencies();

            BindUpdateManagerDependencies();

            Container.Bind<UpdateManager>().FromInstance(updateManager).AsSingle();
        }

        private List<ActorSpawner> FindAllSpawners()
        {
            return FindObjectsOfType<ActorSpawner>().ToList();
        }

        private void SpawnQ(List<ActorSpawner> actorSpawners)
        {
            var qSpawner = actorSpawners.Find(x => x.ActorType == ActorType.Q);
            GameObject Q_GO = Container.InstantiatePrefab(QPrefab, qSpawner.transform.parent);
            Q_GO.transform.position = qSpawner.transform.position;
            Q_GO.name = qSpawner.ActorName;

            var Q = Q_GO.GetComponent<Q>();
            if (Q == null)
            {
                Debug.LogError("Q prefab is not have Q component!", this);
            }
            Q.GravityDirection = qSpawner.InitialGravity;
            Destroy(qSpawner.gameObject);
        }

        private void SpawnBoxes(List<ActorSpawner> actorSpawners)
        {
            foreach (var actorSpawner in actorSpawners)
            {
                if (actorSpawner.ActorType != ActorType.Box)
                {
                    continue;
                }

                GameObject boxGO = Container.InstantiatePrefab(BoxPrefab, actorSpawner.transform.parent);
                boxGO.transform.position = actorSpawner.transform.position;
                boxGO.name = actorSpawner.ActorName;

                var box = boxGO.GetComponent<Box>();
                if (box == null)
                {
                    Debug.LogError("Box prefab is not have Box component!", this);
                }
                box.GravityDirection = actorSpawner.InitialGravity;
                Destroy(actorSpawner.gameObject);
            }
        }

        private void BindQDependencies()
        {
            Container.Bind<Q_Orientation>().FromInstance(QOrientation).AsSingle();
            Container.Bind<IEnvironment>().To<Level>().FromInstance(level).AsSingle();
        }

        private void BindQOrientationDependencies()
        {
            Q_Orientation.ConstructArgs args = new Q_Orientation.ConstructArgs()
            {
                CameraT = mainCamera.transform,
                Settings = orientationSettings,
            };
            Container.Bind<Q_Orientation.ConstructArgs>().FromInstance(args).AsTransient();
        }

        private void BindCameraDependencies()
        {
            CameraBehaviour.ConstructArgs args = new CameraBehaviour.ConstructArgs
            {
                Focus = cameraFocus.transform,
                Orientation = QOrientation,
                Settings = cameraSettings
            };
            Container.Bind<CameraBehaviour.ConstructArgs>().FromInstance(args).AsTransient();
        }

        private void BindCameraFocusDependencies()
        {
            CameraFocus.ConstructArgs args = new CameraFocus.ConstructArgs()
            {
                MainCamera = mainCamera,
                MoveTarget = QOrientation
            };
            Container.Bind<CameraFocus.ConstructArgs>().FromInstance(args).AsTransient();
        }

        private void BindUpdateManagerDependencies()
        {
            var updatables = sceneRoot.GetComponentsInChildren<IUpdatable>().ToList();
            Container.Bind<UpdateManager.ConstructArgs>().FromInstance(new UpdateManager.ConstructArgs()
            {
                UpdatableObjects = updatables
            });
        }
    }
}