using UnityEngine;
using Zenject;

namespace qBox
{
    public class ProjectInstaller : MonoInstaller
    {
        [SerializeField] private GlobalGameSettings gameSettings;

        public override void InstallBindings()
        {
            Container.Bind<GlobalGameSettings>().FromInstance(gameSettings).AsSingle();
        }
    }
}