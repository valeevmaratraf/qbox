﻿namespace qBox.GameBehaviours
{
    public static class UpdatePriorityList
    {
        //========== Post step by step update ========
        public const uint VisabilityRegistration = Camera + 1;
        public const uint Camera = CameraFocus + 1;
        public const uint CameraFocus = QOrientation + 1;
        //============================================

        //============= Step by step =================
        public const uint QOrientation = Q + 1;
        public const uint Q = BoxPush + 1;

        public const uint BoxPush = BoxFall + 1;
        public const uint BoxFall = BoxIdle + 1;
        public const uint BoxIdle = 1;
        //============================================
    }
}