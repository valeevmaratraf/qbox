﻿namespace qBox.GameBehaviours
{
    public interface IStepByStepUpdatable : IUpdatable
    {
        void Discard();
        void SaveState();
    }

    public interface IUpdatable
    {
        TickResult Tick(float simulationTime, bool resimulateAllowance);
        uint UpdatePriority { get; }
        bool Enabled { get; }
    }

    public enum SimpleUpdatableOrder { AfterStepByStep, BeforeStepByStep }
    
    public interface ISimpleUpdatable : IUpdatable
    {
        SimpleUpdatableOrder Order { get; }
    }
}