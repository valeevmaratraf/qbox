﻿namespace qBox.GameBehaviours
{
    public class TickResult
    {
        public float TimeElapsed { get; set; }
        public bool TryResimulate { get; set; }

        public TickResult(float timeElapsed, bool tryResimulate = false)
        {
            TimeElapsed = timeElapsed;
            TryResimulate = tryResimulate;
        }
    }
}