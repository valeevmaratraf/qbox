﻿using qBox.GameBehaviours.Actors;
using qBox.GameBehaviours.GameCamera;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using Zenject;

namespace qBox.GameBehaviours
{
    public class UpdateManager : MonoBehaviour
    {
        [SerializeField] private float descreteSimulationTime = 0.0123F;

        private List<IUpdatable> allUpdatables;
        private List<IUpdatable> frameAddCache;

        private List<IStepByStepUpdatable> stepByStepUpdatables;
        private List<ISimpleUpdatable> simpleUpdatables;


        [Inject]
        private void Construct(ConstructArgs args)
        {
            allUpdatables = args.UpdatableObjects;
            stepByStepUpdatables = allUpdatables.OfType<IStepByStepUpdatable>().ToList();
            simpleUpdatables = allUpdatables.OfType<ISimpleUpdatable>().ToList();
            frameAddCache = new List<IUpdatable>(16);
        }

        private void Update()
        {
            //HACK: первые кадры имеют слишком большые deltaTime'ы
            if (Time.frameCount < 5)
            {
                return;
            }

            simpleUpdatables.Sort((x, y) => (int)y.UpdatePriority - (int)x.UpdatePriority);
            stepByStepUpdatables.Sort((x, y) => (int)y.UpdatePriority - (int)x.UpdatePriority);

            HandleSimpleUpdatables(SimpleUpdatableOrder.BeforeStepByStep);
            UpdateStepByStepUpdatables(0, Time.deltaTime, true);
            HandleSimpleUpdatables(SimpleUpdatableOrder.AfterStepByStep);

            ReplaceFromCacheToUpdatableObjectsLists();
        }

        private void UpdateStepByStepUpdatables(int resimulationNumber, float simulationTimeLeft, bool resimulateAllowed)
        {
            while (simulationTimeLeft > 0)
            {
                SaveStatesOfStepByStepUpdatableObjects();

                float simTime = descreteSimulationTime > simulationTimeLeft ? simulationTimeLeft : descreteSimulationTime;
                if (resimulateAllowed)
                {
                    for (int i = 0; i < resimulationNumber; i++)
                    {
                        simTime /= 2;
                    }
                }

                bool discard = HandleStepByStepUpdate(simTime, resimulateAllowed);

                if (discard && resimulateAllowed)
                {
                    resimulationNumber++;
                    DiscardObjectsStates();
                    resimulateAllowed = simTime > 0.001F;
                    UpdateStepByStepUpdatables(resimulationNumber, simulationTimeLeft, resimulateAllowed);
                    break;
                }
                else
                {
                    simulationTimeLeft -= simTime;
                    resimulationNumber = 0;
                }
            }
        }

        private void DiscardObjectsStates()
        {
            foreach (var obj in stepByStepUpdatables)
            {
                obj.Discard();
            }
        }

        public void HandleSimpleUpdatables(SimpleUpdatableOrder order)
        {
            foreach (var updatable in simpleUpdatables)
            {
                if (!updatable.Enabled || updatable.Order != order)
                {
                    continue;
                }

                updatable.Tick(Time.deltaTime, false);
            }
        }

        private bool HandleStepByStepUpdate(float simulationTime, bool resimulateAllowance)
        {
            for (int i = 0; i < stepByStepUpdatables.Count; i++)
            {
                if (!stepByStepUpdatables[i].Enabled)
                {
                    continue;
                }

                IStepByStepUpdatable obj = stepByStepUpdatables[i];

                float elapsed = 0;
                int stepsCount = 0;
                while (elapsed < simulationTime)
                {
                    var result = obj.Tick(simulationTime - elapsed, resimulateAllowance);
                    if (result.TryResimulate && resimulateAllowance)
                    {
                        return true;
                    }

                    stepsCount++;
                    if (stepsCount > 100)
                    {
                        Debug.LogError($"Too many steps for object! Object: {obj}", this);
                        break;
                    }

                    elapsed += result.TimeElapsed;

                    if (elapsed > simulationTime)
                    {
                        Debug.LogWarning($"Object sumulated more than allowed! " +
                            $"Elapsed simulation time: {result.TimeElapsed}, allowed: {simulationTime}," +
                            $"object: {obj}", this);
                    }
                }
            }
            return false;
        }

        private void SaveStatesOfStepByStepUpdatableObjects()
        {
            foreach (var obj in stepByStepUpdatables)
            {
                obj.SaveState();
            }
        }
        private void ReplaceFromCacheToUpdatableObjectsLists()
        {
            foreach (var obj in frameAddCache)
            {
                if (obj is IStepByStepUpdatable)
                {
                    stepByStepUpdatables.Add(obj as IStepByStepUpdatable);
                }
                else if (obj is ISimpleUpdatable)
                {
                    simpleUpdatables.Add(obj as ISimpleUpdatable);
                }
            }
            frameAddCache.Clear();
        }

        public void RegisterUpdatable(IUpdatable obj)
        {
            if (!(obj is IStepByStepUpdatable || obj is ISimpleUpdatable))
            {
                Debug.LogError("Not recognized updatable interface for registration!", this);
            }

            if (frameAddCache.Contains(obj) || allUpdatables.Contains(obj))
            {
                Debug.LogWarning($"Object already registered! Object: {obj}. Aborting object registration.", this);
                return;
            }

            frameAddCache.Add(obj);
        }

        //TODO: refactor
        public void UnregisterUpdatable(IUpdatable obj)
        {
            if (frameAddCache.Contains(obj))
            {
                frameAddCache.Remove(obj);
            }
            else if (allUpdatables.Contains(obj))
            {
                allUpdatables.Remove(obj);
            }
            else
            {
                Debug.LogWarning($"Cant find object for remove updatable object! Object: {obj}. Aborting unregistration.", this);
            }

            if (obj is ISimpleUpdatable)
            {
                simpleUpdatables.Remove(obj as ISimpleUpdatable);
            }
            if (obj is IStepByStepUpdatable)
            {
                stepByStepUpdatables.Remove(obj as IStepByStepUpdatable);
            }
        }

        public class ConstructArgs
        {
            public List<IUpdatable> UpdatableObjects { get; set; }
        }
    }
}