﻿using UnityEngine;

namespace qBox
{
    [CreateAssetMenu(fileName = "GlobalSettings", menuName = "qBox/Settings/Global")]
    public class GlobalGameSettings : ScriptableObject
    {
        [SerializeField] private float gravityAcceleration = 9.81F;

        public float GravityAcceleration => gravityAcceleration;

        private void OnValidate()
        {
            if (gravityAcceleration <= 0)
            {
                gravityAcceleration = 0.01F;
            }
        }
    }
}