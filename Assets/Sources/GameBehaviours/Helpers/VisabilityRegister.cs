﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace qBox.GameBehaviours
{
    public class VisabilityRegister : ISimpleUpdatable
    {
        private List<Renderer> renderers;

        public event Action Disappeared;
        public event Action Appeared;

        public bool IsVisible { get; private set; }
        public uint UpdatePriority { get; set; } = UpdatePriorityList.VisabilityRegistration;
        public bool Enabled { get; set; }
        public SimpleUpdatableOrder Order => SimpleUpdatableOrder.AfterStepByStep;

        public VisabilityRegister()
        {
            Enabled = true;
            renderers = new List<Renderer>();
        }

        public TickResult Tick(float simulationTime, bool _)
        {
            TickResult tickRes = new TickResult(simulationTime);

            foreach (Renderer r in renderers)
            {
                if (r.isVisible)
                {
                    if (!IsVisible)
                    {
                        IsVisible = true;
                        if (Appeared != null)
                        {
                            Appeared();
                        }
                    }
                    return tickRes;
                }
            }
            if (IsVisible == true)
            {
                IsVisible = false;
                if (Disappeared != null)
                {
                    Disappeared();
                }
            }
            return tickRes;
        }

        public void AddRenderer(Renderer r)
        {
            if (renderers.Contains(r))
            {
                Debug.LogWarning("Trying register renderer which already registered!", r.gameObject);
                return;
            }
            renderers.Add(r);
        }

        public void AddRenderers(Renderer[] rs)
        {
            foreach (var r in rs)
            {
                AddRenderer(r);
            }
        }

        public void RemoveRenderers(Renderer[] rs)
        {
            foreach (var r in rs)
            {
                RemoveRenderer(r);
            }
        }

        public void RemoveRenderer(Renderer r)
        {
            renderers.Remove(r);
        }
    }
}