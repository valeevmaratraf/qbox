﻿namespace qBox.GameBehaviours.Actors
{
    public interface IPushable
    {
        bool IsCanPush(Direction fromTo);
        void SetPushDirection(Direction fromTo);
    }
}