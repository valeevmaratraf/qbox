﻿using qBox.GameBehaviours.qBoxStateMachine;
using qBox.GameBehaviours.qBoxStateMachine.States;
using qBox.GameBehaviours.World;

using System;

using UnityEngine;

namespace qBox.GameBehaviours.Actors
{
    public abstract class ActorBase : MonoBehaviour, IActor
    {
        private Direction gravityDirection;
        private Direction destinationGravity;

        protected Transform ActorT { get; private set; }
        protected StateMachine StateMachine { get; private set; }
        protected Fall FallState { get; private set; }

        public StateType CurrentState => StateMachine.CurrentStateType;

        public abstract uint UpdatePriority { get; }
        public bool Enabled => isActiveAndEnabled;

        public virtual Direction GravityDirection
        {
            get => gravityDirection;
            set
            {
                destinationGravity = value;
                TrySetGravity();
            }
        }

        public Vector3Int Size => Vector3Int.one;
        public Vector3Int GlobalStaticPosition => Vector3Int.FloorToInt(ActorT.position);
        public Quaternion Rotation => ActorT.rotation;
        public Vector3 CenterPosition
        {
            get => ActorT.position;
            set => ActorT.position = value;
        }
        public Collider Collider { get; private set; }
        public virtual bool IsOnGround
        {
            get
            {
                switch (CurrentState)
                {
                    case StateType.Fall:
                        return false;

                    case StateType.Idle:
                        if (!Environment.IsCellWalkable(GlobalStaticPosition + GravityDirection.ToVector()))
                            return true;

                        Vector3Int under = Vector3Int.FloorToInt(CenterPosition + GravityDirection.ToVector());
                        var underActor = Environment.GetActorsAtPosition(under, x =>
                        {
                            if (x == null || x.IsCanEnter(under, GravityDirection))
                            {
                                return false;
                            }
                            return true;

                        }, this);
                        return underActor.Length > 0;

                    default:
                        Debug.LogError("Cant handle IsOnGround for specific state!");
                        return false;
                }
            }
        }
        public virtual Vector3Int[] TakedCells
        {
            get
            {
                switch (CurrentState)
                {
                    case StateType.Fall: return FallState.TakedCells;
                    case StateType.Idle: return new Vector3Int[] { GlobalStaticPosition };
                    default: throw new Exception("Cant return taked cells: unhandled state!");
                }
            }
        }

        public IEnvironment Environment { get; private set; }

        public event Action<IQBoxObject> OnDestroyEvent;
        public event Action<IQBoxObject, bool> OnEnableChange;

        protected virtual void Construct(Collider collider, StateMachine sm, IEnvironment environment)
        {
            Collider = collider;
            StateMachine = sm;
            FallState = sm.FindState<Fall>();
            if (FallState == null)
            {
                Debug.LogError("State machine in ActorBase must have fall state!", this);
            }
            Environment = environment;

            ActorT = transform;
            gravityDirection = destinationGravity;
        }

        protected virtual bool ChooseState()
        {
            if (!IsOnGround)
            {
                StateMachine.SetState(FallState, null, new Fall.EnableSettings(GravityDirection));
                return true;
            }
            return false;
        }

        public virtual TickResult Tick(float simulationTime, bool resimulateAllowance)
        {
            TrySetGravity();

            switch (CurrentState)
            {
                case StateType.Fall:
                    return HandleFallTick(simulationTime, resimulateAllowance);

                case StateType.Idle:
                    Environment.Interact(this);

                    if (ChooseState())
                    {
                        return new TickResult(0);
                    }
                    return new TickResult(simulationTime);

                default:
                    Debug.LogError("Cant handle tick for unhandled state!", this);
                    return null;
            }
        }

        protected virtual TickResult HandleFallTick(float simTime, bool resimulateAllowance)
        {
            Fall.TickArgs args = new Fall.TickArgs(simTime, !resimulateAllowance);
            Fall.TickResult fallTickResult = FallState.Tick(args) as Fall.TickResult;

            var tickResult = new TickResult(simTime);
            tickResult.TimeElapsed = simTime - fallTickResult.TimeLeft;

            if (fallTickResult.IsCollised && resimulateAllowance)
            {
                tickResult.TryResimulate = true;
            }

            if (fallTickResult.Landed)
            {
                StateMachine.SetState(null, null, null);
            }

            if (fallTickResult.OnCellCenter)
            {
                Environment.Interact(this);
            }

            return tickResult;
        }

        public virtual bool IsCanEnter(Vector3Int position, Direction fromTo)
        {
            switch (CurrentState)
            {
                case StateType.Idle:
                    if (position == GlobalStaticPosition)
                    {
                        return false;
                    }
                    break;

                case StateType.Fall:
                    bool parallelDirection = FallState.FallDirection == fromTo || FallState.FallDirection.Flip() == fromTo;
                    bool isTargetCellTaked = false;
                    foreach (var cell in TakedCells)
                    {
                        if (cell == position)
                        {
                            isTargetCellTaked = true;
                        }
                    }

                    if (!parallelDirection && isTargetCellTaked)
                    {
                        return false;
                    }
                    break;
            }
            return true;
        }

        private void TrySetGravity()
        {
            if (gravityDirection == destinationGravity)
            {
                return;
            }

            if (CurrentState == StateType.Idle)
            {
                gravityDirection = destinationGravity;
            }
            else if (CurrentState == StateType.Fall)
            {
                gravityDirection = destinationGravity;
                FallState.FallDirection = destinationGravity;
            }
        }

        private void OnDestroy()
        {
            OnDestroyEvent?.Invoke(this);
        }
        private void OnEnable()
        {
            OnEnableChange?.Invoke(this, true);
        }
        private void OnDisable()
        {
            OnEnableChange?.Invoke(this, false);
        }

        //TODO: write default implementation
        public abstract void SaveState();
        public abstract void Discard();
    }
}