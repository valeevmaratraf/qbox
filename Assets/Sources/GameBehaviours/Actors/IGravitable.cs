﻿namespace qBox.GameBehaviours
{
    public interface IGravitable
    {
        Direction GravityDirection { get; set; }
        bool IsOnGround { get; }
    }
}