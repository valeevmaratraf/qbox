﻿using qBox.GameBehaviours.World;
using UnityEngine;

namespace qBox.GameBehaviours.Actors
{
    public interface IActor : ISolid, IGravitable, IStepByStepUpdatable
    {
        StateType CurrentState { get; }
        IEnvironment Environment { get; }

        bool IsCanEnter(Vector3Int position, Direction fromTo);
    }
}