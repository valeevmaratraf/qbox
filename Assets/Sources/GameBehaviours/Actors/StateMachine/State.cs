﻿using qBox.GameBehaviours.Actors;

namespace qBox.GameBehaviours.qBoxStateMachine.States
{
    public abstract class State
    {
        private StateKeeper keeper;

        public abstract StateType StateType { get; }

        public bool Enabled { get; private set; } = true;

        protected virtual void BeforeEnableActions(EnableSettings enableSettings) { }
        protected virtual void BeforeDisableActions(DisableSettings disableSettings) { }

        public abstract TickResult Tick(TickArgs args);

        public State()
        {
            keeper = new StateKeeper();
        }
        public virtual void SaveState()
        {
            keeper.Enabled = Enabled;
        }
        public virtual void LoadState()
        {
            Enabled = keeper.Enabled;
        }

        public void Enable(EnableSettings settings)
        {
            Enabled = true;
            BeforeEnableActions(settings);
        }
        public void Disable(DisableSettings settings)
        {
            BeforeDisableActions(settings);
            Enabled = false;
        }

        public class EnableSettings
        {
        }
        public class DisableSettings
        {
        }
        public class TickResult
        {
            public float TimeLeft { get; set; }

            public TickResult(float timeLeft = 0)
            {
                TimeLeft = timeLeft;
            }
        }
        public class TickArgs
        {
            public float SimulationTime { get; set; }

            public TickArgs(float simulationTime)
            {
                SimulationTime = simulationTime;
            }
        }

        protected class StateKeeper
        {
            public bool Enabled { get; set; }
        }
    }
}