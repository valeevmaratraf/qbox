﻿using qBox.GameBehaviours.Actors;
using System;
using UnityEngine;

namespace qBox.GameBehaviours.qBoxStateMachine.States
{
    //TODO: Валидация enable settings и прочие защиты от дурака
    //TODO: написать медиатор для отображения состояния в редакторе
    public class Push : State
    {
        private IActor target;
        private float velocity;
        private PushStateKeeper keeper;

        public Vector3Int Original { get; private set; }
        public Direction PushDirection { get; private set; }
        public IActor Pusher { get; private set; }
        public float ReturnAcceleration { get; private set; }
        public override StateType StateType => StateType.Push;
        //TODO: доработать случаи, когда объект занимает только 1 клетку
        public Vector3Int[] TakedCells => new Vector3Int[] { Original, Original + PushDirection.ToVector() };

        public Push(IActor target, GlobalGameSettings settings)
        {
            this.target = target;
            ReturnAcceleration = settings.GravityAcceleration;
            keeper = new PushStateKeeper();
        }

        protected override void BeforeEnableActions(State.EnableSettings enableSettings)
        {
            if (target == null)
            {
                Debug.LogError("Target object for push state is null!");
                return;
            }

            if (target.Size != Vector3Int.one)
            {
                Debug.LogError("Push state cant work with objects which have different than one size!");
                return;
            }

            var pushEnableSettings = enableSettings as EnableSettings;
            PushDirection = pushEnableSettings.PushDirection;
            Pusher = pushEnableSettings.Pusher;
            Original = target.GlobalStaticPosition;
        }

        protected override void BeforeDisableActions(DisableSettings disableSettings)
        {
            target.CenterPosition = Vector3Int.FloorToInt(target.CenterPosition) + (Vector3)target.Size / 2;
        }

        //TODO: доработать расчет прошедшего времени
        public override State.TickResult Tick(TickArgs args)
        {
            float simTime = args.SimulationTime;
            TickResult result = new TickResult(0);

            ResolveInfo info = new ResolveInfo(Pusher, target, PushDirection.GetAxis());
            ChangeReturnBackValues(simTime);

            bool collised = CollisionResolver.ResolveCollision(info);
            if (collised)
            {
                velocity = 0;
            }

            result.IsPushEnded = CheckMaxPush();

            result.Velocity = velocity;
            return result;
        }

        private void ChangeReturnBackValues(float simTime)
        {
            velocity += simTime * ReturnAcceleration;
            Vector3 deltaPos = velocity * (Vector3)PushDirection.Flip().ToVector() * simTime;
            target.CenterPosition += deltaPos;
        }

        private bool CheckMaxPush()
        {
            Vector3 oldPosCenter = Original + (Vector3)target.Size / 2F;
            Vector3 newPosCenter = Original + PushDirection.ToVector() + (Vector3)target.Size / 2F;
            float epsilon = CollisionResolver.Epsilon;

            Vector3 currentPos = target.CenterPosition;
            switch (PushDirection)
            {
                case Direction.Back:
                    if (currentPos.z > oldPosCenter.z || currentPos.z < newPosCenter.z + epsilon)
                    {
                        return true;
                    }
                    break;

                case Direction.Forward:
                    if (currentPos.z < oldPosCenter.z || currentPos.z > newPosCenter.z - epsilon)
                    {
                        return true;
                    }
                    break;

                case Direction.Left:
                    if (currentPos.x > oldPosCenter.x || currentPos.x < newPosCenter.x + epsilon)
                    {
                        return true;
                    }
                    break;

                case Direction.Right:
                    if (currentPos.x < oldPosCenter.x || currentPos.x > newPosCenter.x - epsilon)
                    {
                        return true;
                    }
                    break;

                case Direction.Down:
                    if (currentPos.y > oldPosCenter.y || currentPos.y < newPosCenter.y + epsilon)
                    {
                        return true;
                    }
                    break;
                case Direction.Up:
                    if (currentPos.y < oldPosCenter.y || currentPos.y > newPosCenter.y - epsilon)
                    {
                        return true;
                    }
                    break;
            }
            return false;
        }

        public override void SaveState()
        {
            base.SaveState();
            keeper.Velocity = velocity;
        }
        public override void LoadState()
        {
            base.LoadState();
            velocity = keeper.Velocity;
        }

        private class PushStateKeeper : StateKeeper
        {
            public float Velocity { get; set; }
        }

        new public class EnableSettings : State.EnableSettings
        {
            public IActor Pusher { get; set; }
            public Direction PushDirection { get; set; }
        }

        new public class TickResult : State.TickResult
        {
            public bool IsPushEnded { get; set; }
            public float Velocity { get; set; }
            public bool IsCollisied { get; set; }

            public TickResult(float timeLeft, float velocity = 0, 
                bool isPushEnded = false, bool isCollisied = false) :
                
                base(timeLeft)
            {
                IsPushEnded = isPushEnded;
                Velocity = velocity;
                IsCollisied = isCollisied;
            }
        }
    }
}