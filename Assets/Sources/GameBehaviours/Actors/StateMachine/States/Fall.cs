﻿using qBox.GameBehaviours.Actors;
using System;
using UnityEngine;

namespace qBox.GameBehaviours.qBoxStateMachine.States
{
    //TODO: написать "защиты от дурака"
    //TODO: написать медиатор для отображения состояния в редакторе
    public class Fall : State
    {
        private GlobalGameSettings gameSettings;
        private IActor target;

        private FallStateKeeper keeper;

        public override StateType StateType => StateType.Fall;
        //TODO: Add check for object on cell center
        public Direction FallDirection { get; set; }
        public Vector3 FallDirectionVector3 => FallDirection.ToVector();
        public Vector3Int FallDirectionVector3Int => Vector3Int.RoundToInt(FallDirectionVector3);
        public Vector3Int[] TakedCells
        {
            get
            {
                return new Vector3Int[]
                {
                    Vector3Int.FloorToInt(target.CenterPosition + FallDirectionVector3 / 2),
                    Vector3Int.FloorToInt(target.CenterPosition - FallDirectionVector3 / 2),
                };
            }
        }
        public float VelocityMagnitude { get; set; }
        public float AccelerationMagnitude { get; private set; }

        public Fall(GlobalGameSettings settings, IActor target)
        {
            gameSettings = settings;

            if (target == null)
            {
                Debug.LogError("Target object for fall state is null!");
                return;
            }
            this.target = target;
            keeper = new FallStateKeeper();
        }

        protected override void BeforeEnableActions(State.EnableSettings settings)
        {
            if (target.Size != Vector3Int.one)
            {
                Debug.LogError("Fall state cant work with objects which have different than one size!");
                return;
            }

            var fallSettings = settings as EnableSettings;
            FallDirection = fallSettings.FallDirection;
            AccelerationMagnitude = gameSettings.GravityAcceleration;
            VelocityMagnitude = fallSettings.StartVelocityMagnitude;
        }

        public override void LoadState()
        {
            base.LoadState();
            VelocityMagnitude = keeper.VelocityMagnitude;
            FallDirection = keeper.FallDirection;
            AccelerationMagnitude = keeper.AccelerationMagnitude;
        }
        public override void SaveState()
        {
            base.SaveState();
            keeper.VelocityMagnitude = VelocityMagnitude;
            keeper.FallDirection = FallDirection;
            keeper.AccelerationMagnitude = AccelerationMagnitude;
        }

        public override State.TickResult Tick(State.TickArgs args)
        {
            var fallTickArgs = args as TickArgs;

            var originVelocity = VelocityMagnitude;
            ChangeVelocity(fallTickArgs);

            Vector3 deltaPos = (Vector3)target.GravityDirection.ToVector() * (VelocityMagnitude * fallTickArgs.SimulationTime);
            Vector3 fallCollisionPoint = target.CenterPosition + FallDirectionVector3 / 2;
            Vector3Int checkingCell = Vector3Int.FloorToInt(fallCollisionPoint + deltaPos);
            bool isCellFree = target.Environment.IsCellWalkable(checkingCell);

            if (!isCellFree)
            {
                return new TickResult(true, Land(fallTickArgs.SimulationTime, checkingCell), true);
            }

            var actorsOnPath = target.Environment.GetActorsAtPosition(checkingCell, null, target);

            bool canEnter = true;
            foreach (var actor in actorsOnPath)
            {
                if (!actor.IsCanEnter(checkingCell, FallDirection))
                {
                    canEnter = false;
                    break;
                }
            }

            if (!canEnter)
            {
                return new TickResult(true, Land(fallTickArgs.SimulationTime, checkingCell), true);
            }
            else
            {
                float elapsedSimulationTime = CheckCellCenterIntersecting(deltaPos, originVelocity);
                if (elapsedSimulationTime < 0)
                {
                    Debug.LogError($"Negative simulation time left after cell center intersection: {elapsedSimulationTime}.");
                }

                if (elapsedSimulationTime > 0)
                {
                    return new TickResult(false, fallTickArgs.SimulationTime - elapsedSimulationTime, true);
                }

                //TODO: реализовать учет больше 1 actor'а
                if (actorsOnPath.Length > 0)
                {
                    bool isCollisied = CheckCollision(actorsOnPath);
                    if (isCollisied)
                    {
                        if (fallTickArgs.TryResolveCollision)
                        {
                            ResolveInfo info = new ResolveInfo(actorsOnPath[0], target, FallDirection.GetAxis());
                            if (CollisionResolver.ResolveCollision(info))
                            {
                                //TODO: сделать более реалистичное "противодействие"
                                //при столкновении
                                return new TickResult(false, 0);
                            }

                            return new TickResult(false, 0);
                        }
                        else
                        {
                            return new TickResult(false, 0, false, true);
                        }
                    }
                    else
                    {
                        return new TickResult(false, 0);
                    }
                }
                else
                {
                    return new TickResult(false, 0);
                }
            }
        }

        private float CheckCellCenterIntersecting(Vector3 deltaPos, float originVelocity)
        {
            Vector3 oldPosV = target.CenterPosition;
            target.CenterPosition += deltaPos;
            Vector3 newPosV = target.CenterPosition;

            Axis axis = FallDirection.GetAxis();
            float oldPos = oldPosV.ExtractComponent(axis);
            float newPos = newPosV.ExtractComponent(axis);

            bool intersectedCenter = false;
            float movedDistanceBeforeCross = 0;

            if (oldPos < 0 && newPos > 0 || oldPos > 0 && newPos < 0)
            {
                intersectedCenter = false;
            }
            else if (FallDirection.GetSign() == 1)
            {
                if (oldPos > 0 && newPos > 0 && oldPos % 1 < 0.5F && newPos % 1 > 0.5F)
                {
                    target.CenterPosition = target.GlobalStaticPosition + (Vector3)target.Size / 2;
                    movedDistanceBeforeCross = 0.5F - oldPos % 1;
                    intersectedCenter = true;
                }
                else if (oldPos < 0 && newPos < 0 && oldPos % 1 < -0.5F && newPos % 1 > -0.5F)
                {
                    target.CenterPosition = target.GlobalStaticPosition + (Vector3)target.Size / 2;
                    movedDistanceBeforeCross = Mathf.Abs(oldPos % 1) - 0.5F;
                    intersectedCenter = true;
                }
            }
            else if (FallDirection.GetSign() == -1)
            {
                if (oldPos > 0 && newPos > 0 && oldPos % 1 > 0.5F && newPos % 1 < 0.5F)
                {
                    target.CenterPosition = target.GlobalStaticPosition + (Vector3)target.Size / 2;
                    movedDistanceBeforeCross = oldPos % 1 - 0.5F;
                    intersectedCenter = true;
                }
                else if (oldPos < 0 && newPos < 0 && oldPos % 1 > -0.5F && newPos % 1 < -0.5F)
                {
                    target.CenterPosition = target.GlobalStaticPosition + (Vector3)target.Size / 2;
                    movedDistanceBeforeCross = 0.5F - Mathf.Abs(oldPos % 1);
                    intersectedCenter = true;
                }
            }

            if (intersectedCenter)
            {
                VelocityMagnitude = Mathf.Sqrt(2 * movedDistanceBeforeCross * AccelerationMagnitude + originVelocity * originVelocity);
                var elapsedTime = movedDistanceBeforeCross / VelocityMagnitude;
                return elapsedTime;
            }

            return 0;
        }

        private void ChangeVelocity(State.TickArgs args)
        {
            float deltaVelocity = args.SimulationTime * AccelerationMagnitude;
            VelocityMagnitude += deltaVelocity;
        }

        private bool CheckCollision(IActor[] actorsOnPath)
        {
            foreach (var actor in actorsOnPath)
            {
                CollisionResolverCalcInfo calcInfo = new CollisionResolverCalcInfo(actor, target);
                var result = CollisionResolver.CalcCollision(calcInfo);
                if (result != null)
                {
                    return true;
                }
            }
            return false;
        }

        private float Land(float simulationTime, Vector3Int landCell)
        {
            Vector3 landPos = landCell + (Vector3)target.Size / 2 - FallDirectionVector3;
            float lastFallPartDist = Vector2.Distance(target.CenterPosition, landPos);
            target.CenterPosition = landPos;
            float timeLeft = simulationTime - lastFallPartDist / VelocityMagnitude;
            return timeLeft;
        }

        new public class TickArgs : State.TickArgs
        {
            public bool TryResolveCollision { get; set; }

            public TickArgs(float simulationTime, bool tryResolveCollision)
                : base(simulationTime)
            {
                TryResolveCollision = tryResolveCollision;
            }
        }

        private class FallStateKeeper : StateKeeper
        {
            public float VelocityMagnitude { get; set; }
            public Direction FallDirection { get; set; }
            public float AccelerationMagnitude { get; set; }
            public Direction TargetFallDirection { get; set; }
        }

        new public class EnableSettings : State.EnableSettings
        {
            public float StartVelocityMagnitude { get; set; }
            public Direction FallDirection { get; set; }

            public EnableSettings(Direction fallDirection, float startVelocity = 0)
            {
                FallDirection = fallDirection;
                StartVelocityMagnitude = startVelocity;
            }
        }

        new public class TickResult : State.TickResult
        {
            public bool Landed { get; set; }
            public bool OnCellCenter { get; set; }
            public bool IsCollised { get; set; }

            public TickResult(bool landed, float timeLeft, bool onCellCenter = false, bool isCollised = false)
                : base(timeLeft)
            {
                Landed = landed;
                OnCellCenter = onCellCenter;
                IsCollised = isCollised;
            }
        }
    }
}