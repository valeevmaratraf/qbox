﻿using qBox.GameBehaviours.Actors;
using System;
using UnityEngine;

namespace qBox.GameBehaviours.qBoxStateMachine.States
{
    //TODO: избавиться от MaxAngle, использовать анализ через IEnviroment
    //TODO: написать медиатор для отображения состояния в редакторе
    public class Roll : State
    {
        public enum RotationForceType { OnlyGravity = 0, Inversed = -1, Normal = 1, }
        public delegate void RotateAroundMethod(Vector3 rotationPoint, Vector3 RotationAxis, float angle);

        private RollStateKeeper keeper;
        private IActor target;
        private GlobalGameSettings gameSettings;
        private RotateAroundMethod rotateMethod;

        public override StateType StateType => StateType.Roll;

        private Vector3Int up, forward;
        public float DeltaAngle { get; private set; }
        public Vector3 RotationAxis { get; private set; }
        public Vector3 RotationPoint { get; private set; }
        public float AngularVelocity { get; private set; }
        public float AngularAccelerationMagnitude { get; private set; }
        public float MaxAngle { get; private set; }
        public Direction ForwardDirection { get; private set; }
        public Vector3Int[] TakedCells
        {
            get
            {
                if (DeltaAngle == 0)
                {
                    return new Vector3Int[] { target.GlobalStaticPosition };
                }

                if (DeltaAngle == 90)
                {
                    return new Vector3Int[] { target.GlobalStaticPosition + forward };
                }

                var halfUp = (Vector3)up / 2;
                var halfFwd = (Vector3)forward / 2;

                return new Vector3Int[]
                {
                    Vector3Int.FloorToInt(RotationPoint + halfUp + halfFwd),
                    Vector3Int.FloorToInt(RotationPoint + halfUp - halfFwd),
                    Vector3Int.FloorToInt(RotationPoint + halfUp * 3 + halfFwd),
                    Vector3Int.FloorToInt(RotationPoint + halfUp * 3 - halfFwd)
                };
            }
        }

        public Roll(IActor target, GlobalGameSettings settings, RotateAroundMethod rotateMethod)
        {
            this.target = target;
            gameSettings = settings;
            this.rotateMethod = rotateMethod;
            keeper = new RollStateKeeper();
        }

        protected override void BeforeEnableActions(State.EnableSettings settings)
        {
            if (target == null)
            {
                Debug.LogError("Target object for fall state is null!");
                return;
            }

            if (target.Size != Vector3Int.one)
            {
                Debug.LogError("Fall state cant work with objects which have different than one size!");
                target = null;
                return;
            }

            if (rotateMethod == null)
            {
                Debug.LogError($"The delegate for roll ({nameof(rotateMethod)}) is null!");
            }

            EnableSettings rollSettings = ValidateSettings(settings);

            AngularAccelerationMagnitude = rollSettings.AngularAccelerationMagnitude;
            AngularVelocity = rollSettings.AngularVelocity;
            MaxAngle = rollSettings.MaxAngle;
            DeltaAngle = 0;

            up = rollSettings.Up;
            forward = rollSettings.Forward;
            ForwardDirection = forward.ToEnum();

            RotationAxis = Vector3Int.RoundToInt(Vector3.Cross(up, forward).normalized);
            RotationPoint = target.CenterPosition - (Vector3)up / 2 + (Vector3)forward / 2;
        }
        private static EnableSettings ValidateSettings(State.EnableSettings settings)
        {
            if (settings == null)
            {
                Debug.LogError("Enable settings for setted 'Roll' state is null!");
                return null;
            }

            if (!(settings is EnableSettings))
            {
                Debug.LogError("Setted state 'Roll' with invalid type of settings!");
                return null;
            }

            EnableSettings rollSettings = settings as EnableSettings;

            if (rollSettings.MaxAngle <= 0)
            {
                Debug.LogError("The maximum angle for the roll state is less than " +
                                            "or equal to 0! This can lead to improper " +
                                            "operation of the state.");
                return null;
            }

            return rollSettings;
        }

        public override State.TickResult Tick(State.TickArgs args)
        {
            RotationForceType rotationForce;
            if (args is TickArgs)
            {
                rotationForce = (args as TickArgs).RotationForce;
            }
            else
            {
                Debug.LogError("State args for Roll.Tick() is not a Roll.TickArgs!");
                return null;
            }

            float simulationTime = args.SimulationTime;
            float tickDeltaAngle = ChangeRollParametres(simulationTime, rotationForce);
            rotateMethod(RotationPoint, RotationAxis, tickDeltaAngle);

            if (DeltaAngle >= MaxAngle)
            {
                return CompliteState(true);
            }
            else if (DeltaAngle <= 0)
            {
                return CompliteState(false);
            }
            else
            {
                return new TickResult(false, 0);
            }
        }
        private TickResult CompliteState(bool isFullStep)
        {
            float TimeLeft;
            if (isFullStep)
            {
                float overDeltaAngle = DeltaAngle - MaxAngle;
                TimeLeft = overDeltaAngle * Mathf.Deg2Rad / AngularVelocity;
                rotateMethod(RotationPoint, RotationAxis, MaxAngle - DeltaAngle);
                DeltaAngle = MaxAngle;
            }
            else
            {
                TimeLeft = DeltaAngle * Mathf.Deg2Rad / AngularVelocity;
                rotateMethod(RotationPoint, RotationAxis, -DeltaAngle);
                DeltaAngle = 0;
            }
            return new TickResult(true, TimeLeft);
        }
        private float ChangeRollParametres(float simulationTime, RotationForceType rotationForce)
        {
            float currentTickAngularAcceleration = AngularAccelerationMagnitude;
            if (rotationForce == RotationForceType.Inversed)
            {
                currentTickAngularAcceleration *= -1;
            }
            else if (rotationForce == RotationForceType.OnlyGravity)
            {
                currentTickAngularAcceleration = 0;
            }

            //TODO: сделать обработку влияния гравитации при углах больших, чем 90 градусов
            AddGravityCorrection(ref currentTickAngularAcceleration);

            AngularVelocity += simulationTime * currentTickAngularAcceleration;
            float tickDeltaAngle = AngularVelocity * simulationTime * Mathf.Rad2Deg;
            DeltaAngle += tickDeltaAngle;
            return tickDeltaAngle;
        }
        private void AddGravityCorrection(ref float currentForce)
        {
            float hypotenuse, topSide, gravityForce;
            float gravityMagnitude = gameSettings.GravityAcceleration;
            if (DeltaAngle < MaxAngle * 0.5F)
            {
                hypotenuse = 1 / Mathf.Cos(Mathf.Deg2Rad * DeltaAngle);
                topSide = Mathf.Sin(Mathf.Deg2Rad * DeltaAngle) * hypotenuse;
                gravityForce = -gravityMagnitude * (1 - 0.5F * topSide * 1);
            }
            else
            {
                hypotenuse = 1 / Mathf.Cos(Mathf.Deg2Rad * (MaxAngle - DeltaAngle));
                topSide = Mathf.Sin(Mathf.Deg2Rad * (MaxAngle - DeltaAngle)) * hypotenuse;
                gravityForce = gravityMagnitude * (1 - 0.5F * topSide * 1);
            }
            if (target.IsOnGround)
            {
                currentForce += gravityForce;
            }
            else
            {
                currentForce = gravityForce;
            }
        }

        public override void SaveState()
        {
            base.SaveState();
            keeper.DeltaAngle = DeltaAngle;
            keeper.RotationAxis = RotationAxis;
            keeper.RotationPoint = RotationPoint;
            keeper.AngularVelocity = AngularVelocity;
            keeper.AngularAccelerationMagnitude = AngularAccelerationMagnitude;
            keeper.MaxAngle = MaxAngle;
            keeper.ForwardDirection = ForwardDirection;
        }

        public override void LoadState()
        {
            base.LoadState();
            DeltaAngle = keeper.DeltaAngle;
            RotationAxis = keeper.RotationAxis;
            RotationPoint = keeper.RotationPoint;
            AngularVelocity = keeper.AngularVelocity;
            AngularAccelerationMagnitude = keeper.AngularAccelerationMagnitude;
            MaxAngle = keeper.MaxAngle;
            ForwardDirection = keeper.ForwardDirection;
        }

        new public class EnableSettings : State.EnableSettings
        {
            public Vector3Int Forward { get; set; }
            public Vector3Int Up { get; set; }
            public float AngularVelocity { get; set; }
            public float MaxAngle { get; set; }
            public float AngularAccelerationMagnitude { get; set; }

            public EnableSettings(Vector3Int forward, Vector3Int up, float angularVelociyu,
                                  float angularAccelerationMagnitude, float maxAngle = 90)
            {
                AngularVelocity = angularVelociyu;
                MaxAngle = maxAngle;
                AngularAccelerationMagnitude = angularAccelerationMagnitude;
                Forward = forward;
                Up = up;
            }
        }
        new public class TickResult : State.TickResult
        {
            public bool IsRollEnded { get; set; }

            public TickResult(bool ended = false, float TimeLeft = 0)
                : base(TimeLeft)
            {
                IsRollEnded = ended;
            }
        }
        new public class TickArgs : State.TickArgs
        {
            public RotationForceType RotationForce { get; set; }

            public TickArgs(float simulationTime, RotationForceType rotationForce = RotationForceType.Normal)
                : base(simulationTime)
            {
                RotationForce = rotationForce;
            }
        }

        private class RollStateKeeper : StateKeeper
        {
            public float DeltaAngle { get; set; }
            public Vector3 RotationAxis { get; set; }
            public Vector3 RotationPoint { get; set; }
            public float AngularVelocity { get; set; }
            public float AngularAccelerationMagnitude { get; set; }
            public float MaxAngle { get; set; }
            public Direction ForwardDirection { get; set; }
        }
    }
}