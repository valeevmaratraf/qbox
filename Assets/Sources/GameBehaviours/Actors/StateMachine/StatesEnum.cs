﻿namespace qBox.GameBehaviours.Actors
{
    public enum StateType
    {
        Idle,
        Roll,
        Fall,
        Push
    }
}