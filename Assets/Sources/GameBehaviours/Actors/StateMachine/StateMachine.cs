﻿using qBox.GameBehaviours.Actors;
using qBox.GameBehaviours.qBoxStateMachine.States;
using System.Collections.Generic;
using UnityEngine;

namespace qBox.GameBehaviours.qBoxStateMachine
{
    public class StateMachine
    {
        private List<State> states;
        private SMKeeper keeper;

        public StateType CurrentStateType
        {
            get
            {
                if (CurrentState == null)
                {
                    return StateType.Idle;
                }
                return CurrentState.StateType;
            }
        }
        public StateType OldStateType
        {
            get
            {
                if (OldState == null)
                {
                    return StateType.Idle;
                }
                return OldState.StateType;
            }
        }

        public State CurrentState { get; private set; }
        public State OldState { get; private set; }

        public StateMachine(List<State> states)
        {
            this.states = states;
            if (states.Count == 0)
            {
                Debug.LogWarning("State machine without any states!");
            }
            keeper = new SMKeeper();
        }
        public State.TickResult Tick(State.TickArgs args)
        {
            if (CurrentState != null)
            {
                return CurrentState.Tick(args);
            }
            return null;
        }

        public T FindState<T>() where T : State
        {
            State state = states.Find(x => x is T);
            if (state == null)
            {
                Debug.LogError(string.Format("Could not find state ({0})!", typeof(T)));
            }

            return state as T;
        }
        public void SetState(State newState, State.DisableSettings disableSettings, State.EnableSettings enableSettings)
        {
            if (CurrentState != null)
            {
                OldState = CurrentState;
            }

            if (OldState != null)
            {
                CurrentState?.Disable(disableSettings);
            }

            CurrentState = newState;
            newState?.Enable(enableSettings);
        }
        public void AddState(State newState)
        {
            states.Add(newState);
        }
        public void RemoveState(State removingState)
        {
            states.Remove(removingState);
        }

        public void SaveState(bool includeStates = true)
        {
            keeper.States = new List<State>(states);
            keeper.CurrentState = CurrentState;
            keeper.OldState = OldState;

            foreach (State s in states)
            {
                s.SaveState();
            }
        }
        public void LoadState(bool includeStates = true)
        {
            states = keeper.States;
            CurrentState = keeper.CurrentState;
            OldState = keeper.OldState;

            foreach (State s in states)
            {
                s.LoadState();
            }
        }

        private class SMKeeper
        {
            public List<State> States { get; set; }
            public State CurrentState { get; set; }
            public State OldState { get; set; }
        }
    }
}