﻿using UnityEngine;

namespace qBox.GameBehaviours.Actors
{
    public enum ActorType { Q, Box }
    public class ActorSpawner : MonoBehaviour
    {
        [SerializeField] private Direction initialGravity;
        [SerializeField] private string actorName;
        [SerializeField] private ActorType actorType;

        public Direction InitialGravity => initialGravity;
        public string ActorName => actorName;
        public ActorType ActorType => actorType;
    }
}