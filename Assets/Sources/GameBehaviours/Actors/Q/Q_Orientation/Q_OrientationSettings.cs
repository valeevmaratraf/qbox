﻿using UnityEngine;

namespace qBox.GameBehaviours.Actors
{
    [CreateAssetMenu(fileName = "Q_OrientationSettings", menuName = "qBox/Settings/Q_Orientation")]
    public class Q_OrientationSettings : ScriptableObject
    {
        [SerializeField] private float changeOrientationTime = 1F;

        public float ChangeOrientationTime => changeOrientationTime;
    }
}
