using qBox.GameBehaviours.qBoxStateMachine.States;
using UnityEngine;

namespace qBox.GameBehaviours
{
    public interface IOrientation
    {
        public Pose OrientationPose { get; }
    }
}
