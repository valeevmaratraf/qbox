﻿using qBox.GameBehaviours.GameCamera;
using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using Zenject;

namespace qBox.GameBehaviours.Actors
{
    [DisallowMultipleComponent, RequireComponent(typeof(Follower))]
    public class Q_Orientation : MonoBehaviour, IStepByStepUpdatable, ICameraFocusTarget, IOrientation
    {
        private Transform cameraT;
        private Transform orientationT;
        private Q_OrientationSettings settings;
        private Follower follower;

        private StateKeeper keeper;

        private Direction destinationDown;
        private Coroutine rotater;
        private Vector3 lastStableUpVector;
        private Quaternion destinationRotation;

        private bool isForwardOrientationChanged;

        public event Action<Transform> OnTeleportStart;
        public event Action<Transform> OnTeleportEnd;

        public Pose CurrentPose => new Pose(orientationT.position, orientationT.rotation);
        public Pose OrientationPose => new Pose(orientationT.position, destinationRotation);
        public bool IsRotating { get; private set; }
        public Vector3 Position => orientationT.position;

        public Transform FollowTarget
        {
            get => follower.FollowTarget;
            set => follower.FollowTarget = value;
        }

        public bool Enabled => isActiveAndEnabled;
        public uint UpdatePriority { get; set; } = UpdatePriorityList.QOrientation;

        [Inject]
        public void Construct(ConstructArgs args)
        {
            cameraT = args.CameraT;
            settings = args.Settings;

            follower = GetComponent<Follower>();
            orientationT = transform;

            keeper = new StateKeeper();
        }

        public TickResult Tick(float simTime, bool _)
        {
            if (!IsRotating)
            {
                SetForwardDirection();
            }
            follower.Move();
            return new TickResult(simTime);
        }

        public void SetDownDirection(Direction downDirection, bool instantly = false)
        {
            if (downDirection == destinationDown)
            {
                return;
            }

            destinationDown = downDirection;

            //Here is magic... IDK how it works...
            if (!IsRotating)
            {
                lastStableUpVector = orientationT.worldToLocalMatrix.MultiplyVector(orientationT.up);
            }
            else
            {
                IsRotating = false;
                if (rotater != null)
                {
                    StopCoroutine(rotater);
                }
            }

            Vector3 up = downDirection.Flip().ToVector();
            destinationRotation = Quaternion.FromToRotation(lastStableUpVector, up);
            if (instantly)
            {
                orientationT.rotation = destinationRotation;
            }
            else
            {
                rotater = StartCoroutine(Rotator());
            }
        }

        private void SetForwardDirection()
        {
            Transform[] children = GetChilds();
            Unparent(children);

            float cameraSteep = Vector3.Angle(cameraT.up, orientationT.up);
            Quaternion cameraUnsteepedRotation = cameraT.rotation * Quaternion.AngleAxis(-cameraSteep, Vector3.right);
            Pose unsteepedCameraPose = new Pose(cameraT.position, cameraUnsteepedRotation);

            float angle;
            do
            {
                angle = Vector3.SignedAngle(orientationT.forward, unsteepedCameraPose.forward, orientationT.up);
                if (angle > 45 || angle < -45)
                {
                    RotateAroundUp90(RotateDirection.Clockwise);
                    isForwardOrientationChanged = true;
                }
                else
                {
                    break;
                }
            }
            while (true);

            Reparent(children);
        }

        private void Reparent(Transform[] children)
        {
            foreach (Transform child in children)
            {
                child.SetParent(orientationT, true);
            }
        }

        private static void Unparent(Transform[] children)
        {
            foreach (Transform child in children)
            {
                child.parent = null;
            }
        }

        private Transform[] GetChilds()
        {
            return GetComponentsInChildren<Transform>().Where(t => t.parent == orientationT).ToArray();
        }

        private IEnumerator Rotator()
        {
            IsRotating = true;
            float timer = 0;
            float progress = 0;
            Quaternion wasRotation = orientationT.rotation;

            while (progress != 1)
            {
                timer += Time.deltaTime;
                progress = timer / settings.ChangeOrientationTime;
                progress = Mathf.Clamp01(progress);
                orientationT.rotation = Quaternion.Slerp(wasRotation, destinationRotation, progress);
                yield return new WaitForEndOfFrame();
            }
            IsRotating = false;
        }
        private void RotateAroundUp90(RotateDirection rotateDirection)
        {
            if (rotateDirection == RotateDirection.Clockwise)
            {
                orientationT.rotation *= Quaternion.AngleAxis(90, Vector3.up);
            }
            else if (rotateDirection == RotateDirection.Anticlockwise)
            {
                orientationT.rotation *= Quaternion.AngleAxis(-90, Vector3.up);
            }
            destinationRotation = orientationT.rotation;
        }

        public void Discard()
        {
            Transform[] childs = null;
            if (keeper.IsForwardOrientationChanged)
            {
                childs = GetChilds();
                Unparent(childs);
            }

            orientationT.position = keeper.Pose.position;
            orientationT.rotation = keeper.Pose.rotation;

            lastStableUpVector = keeper.LastStableUpVector;
            IsRotating = keeper.IsRotating;
            FollowTarget = keeper.FollowTarget;
            destinationRotation = keeper.DestinationRotation;

            if (keeper.IsForwardOrientationChanged)
            {
                Reparent(childs);
            }
        }

        public void SaveState()
        {
            keeper.Pose = new Pose(orientationT.position, orientationT.rotation);

            keeper.LastStableUpVector = lastStableUpVector;
            keeper.IsRotating = IsRotating;
            keeper.FollowTarget = FollowTarget;
            keeper.DestinationRotation = destinationRotation;
            keeper.IsForwardOrientationChanged = isForwardOrientationChanged;
        }

        public void Teleport(Direction newDownDirection)
        {
            OnTeleportStart?.Invoke(orientationT);

            destinationDown = newDownDirection;

            Vector3 up = newDownDirection.Flip().ToVector();
            destinationRotation = Quaternion.FromToRotation(lastStableUpVector, up);
            orientationT.rotation = destinationRotation;

            Vector3 oldPos = orientationT.position;
            follower.Move();
            Vector3 newPos = orientationT.position;
            SetForwardDirection();

            OnTeleportEnd?.Invoke(orientationT);
        }

        public class ConstructArgs
        {
            public Transform CameraT { get; set; }
            public Q_OrientationSettings Settings { get; set; }
        }

        private class StateKeeper
        {
            public Vector3 LastStableUpVector { get; set; }
            public Quaternion DestinationRotation { get; set; }
            public bool IsRotating { get; set; }
            public Transform FollowTarget { get; set; }
            public Pose Pose { get; set; }
            public bool IsForwardOrientationChanged { get; set; }
        }
    }
}
