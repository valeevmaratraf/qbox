using qBox.GameBehaviours.qBoxStateMachine.States;
using UnityEngine;

namespace qBox.GameBehaviours
{
    public interface IRollable
    {
        Roll RollState { get; }
        bool IsRollContinue { get; }
        Vector3Int StartPositionForRoll { get; }
        IOrientation Orientation { get; }
    }
}
