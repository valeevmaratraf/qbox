using qBox.GameBehaviours.Actors;
using qBox.GameBehaviours.qBoxStateMachine.States;
using qBox.GameBehaviours.World;
using qBox.Settings;
using UnityEngine;
using static Zenject.Asteroids.GameSettingsInstaller;

namespace qBox.GameBehaviours
{
    public class StepAnalyzer
    {
        private IRollable rollable;
        private IEnvironment environment;
        private IRollSettings settings;
        private IOrientation orientation;

        public StepAnalyzer(IEnvironment environment, IRollable rollable, IRollSettings settings, IOrientation orientation)
        {
            this.rollable = rollable;
            this.settings = settings;
            this.environment = environment;
            this.orientation = orientation;
        }

        private Roll.EnableSettings GetRollSettings(Vector3Int forward, Vector3Int up)
        {
            Direction moveDirection = forward.ToEnum();
            float startAngularVelocity = 0;

            bool isRollDirectionSaved = rollable.IsRollContinue && rollable.RollState.ForwardDirection == moveDirection;

            if (isRollDirectionSaved)
            {
                startAngularVelocity = rollable.RollState.AngularVelocity * settings.StepContinueCoef;
            }

            return new Roll.EnableSettings(forward, up, startAngularVelocity, settings.StepAngularForce);
        }

        public StepInfo GetStepInfo(Vector2 input)
        {
            if (input == Vector2.zero)
            {
                return null;
            }

            Vector3Int fwd = GetRollDirection(input);
            Vector3Int up = Vector3Int.RoundToInt(orientation.OrientationPose.up);

            Vector3Int fwdPos = fwd + rollable.StartPositionForRoll;
            Vector3Int fwdUpPos = fwd + up + rollable.StartPositionForRoll;
            Vector3Int upPos = up + rollable.StartPositionForRoll;

            if (!environment.IsCellWalkable(fwdPos) ||
                !environment.IsCellWalkable(upPos) ||
                !environment.IsCellWalkable(fwdUpPos))
            {
                return null;
            }

            IActor[] fwdObjects = environment.GetActorsAtPosition(fwdPos);
            IActor[] upObjects = environment.GetActorsAtPosition(upPos);
            IActor[] fwdUpObjects = environment.GetActorsAtPosition(fwdUpPos);

            bool canPushFwd = IsCanPushObjects(fwdObjects, fwd.ToEnum());
            bool canPushUp = IsCanPushObjects(upObjects, up.ToEnum());
            bool canPushFwdUpToFwd = IsCanPushObjects(fwdUpObjects, fwd.ToEnum());
            bool canPushFwdUpToUp = IsCanPushObjects(fwdUpObjects, up.ToEnum());

            if (!canPushFwd || !canPushUp || !(canPushFwdUpToUp || canPushFwdUpToFwd))
            {
                return null;
            }

            SetUpPushObjects(fwdObjects, fwd.ToEnum());
            SetUpPushObjects(upObjects, up.ToEnum());
            if (canPushFwdUpToFwd)
            {
                SetUpPushObjects(fwdUpObjects, fwd.ToEnum());
            }
            if (canPushFwdUpToUp)
            {
                SetUpPushObjects(fwdUpObjects, up.ToEnum());
            }

            Roll.EnableSettings settings = GetRollSettings(fwd, up);
            return new StepInfo(settings, fwd.ToEnum());
        }

        private bool IsCanPushObjects(IActor[] actors, Direction dir)
        {
            foreach (var obj in actors)
            {
                if (!(obj is IPushable))
                {
                    return false;
                }
                var pushableObj = obj as IPushable;
                if (!pushableObj.IsCanPush(dir))
                {
                    return false;
                }
            }
            return true;
        }

        private void SetUpPushObjects(IActor[] actors, Direction dir)
        {
            foreach (var obj in actors)
            {
                if (obj is IPushable)
                {
                    (obj as IPushable).SetPushDirection(dir);
                }
            }
        }

        public Vector3Int GetRollDirection(Vector2 input)
        {
            if (input.y > 0) return Vector3Int.RoundToInt(orientation.OrientationPose.forward);
            else if (input.y < 0) return Vector3Int.RoundToInt(-orientation.OrientationPose.forward);
            else if (input.x > 0) return Vector3Int.RoundToInt(orientation.OrientationPose.right);
            else if (input.x < 0) return Vector3Int.RoundToInt(-orientation.OrientationPose.right);
            return Vector3Int.zero;
        }
    }
}
