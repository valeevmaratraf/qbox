using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace qBox.Settings
{
    public interface IRollSettings
    {
        public float StepAngularForce { get; }
        public float StepContinueCoef { get; }
    }
}
