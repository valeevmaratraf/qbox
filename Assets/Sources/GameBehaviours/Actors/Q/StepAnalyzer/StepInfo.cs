﻿using qBox.GameBehaviours.qBoxStateMachine.States;

namespace qBox.GameBehaviours
{
    public class StepInfo
    {
        public Roll.EnableSettings RollSettings { get; set; }
        public Direction ForwardDirection { get; set; }

        public StepInfo(Roll.EnableSettings settings, Direction forwardDirection)
        {
            RollSettings = settings;
            ForwardDirection = forwardDirection;
        }
    }
}
