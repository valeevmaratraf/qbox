using qBox.GameBehaviours.qBoxStateMachine;
using qBox.GameBehaviours.qBoxStateMachine.States;
using qBox.Settings;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace qBox.GameBehaviours.Actors
{
    [RequireComponent(typeof(Q))]
    public class Q_Installer : MonoInstaller
    {
        [SerializeField] private GlobalGameSettings settings;
        [SerializeField] private Q_Settings Q_Settings;

        public override void InstallBindings()
        {
            StateMachine sm = CreateStateMachine();
            Container.Bind<StateMachine>().FromInstance(sm).AsSingle();
            Container.Bind<Collider>().To<BoxCollider>().FromComponentInChildren().AsSingle();
            Container.Bind<Q_Settings>().FromInstance(Q_Settings).AsSingle();
        }

        //TODO: ���������� � �������������� DI
        private StateMachine CreateStateMachine()
        {
            var Q = GetComponent<Q>();
            List<State> states = new List<State>
            {
                new Roll(Q, settings, transform.RotateAround),
                new Fall(settings, Q)
            };

            StateMachine sm = new StateMachine(states);
            return sm;
        }
    }
}