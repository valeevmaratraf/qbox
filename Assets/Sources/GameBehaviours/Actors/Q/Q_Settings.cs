﻿using UnityEngine;

namespace qBox.Settings
{
    [CreateAssetMenu(fileName = "Q_Settings", menuName = "qBox/Settings/Q")]
    public class Q_Settings : ScriptableObject, IRollSettings
    {
        [SerializeField] private float stepAngularForce = 15;
        [SerializeField] private float stepContinueCoef = 1 / 1.41F;
        [SerializeField] private int underSpawnHeight = 10;

        public float StepAngularForce => stepAngularForce;
        public float StepContinueCoef => stepContinueCoef;
        public float UnderSpawnHeight => underSpawnHeight;

        public void OnValidate()
        {
            if (stepAngularForce < 0)
            {
                stepAngularForce = 0;
            }

            if (stepContinueCoef < 0)
            {
                stepContinueCoef = 0;
            }
        }
    }
}