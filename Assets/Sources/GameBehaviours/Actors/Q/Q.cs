﻿using qBox.GameBehaviours.qBoxStateMachine;
using qBox.GameBehaviours.qBoxStateMachine.States;
using qBox.GameBehaviours.World;
using qBox.Settings;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace qBox.GameBehaviours.Actors
{
    //TODO: написать и интегрировать InputManager
    //TODO: исправить мгновенный респавн, если при старте Q в воздухе
    [DisallowMultipleComponent, SelectionBase]
    public class Q : ActorBase, IActor, IPusher, IRollable
    {
        private Q_Orientation orientation;
        private Q_Settings settings;
        private StepAnalyzer stepAnalyzer;

        private bool respawned;
        private StateKeeper keeper;
        private Roll rollState;
        private Vector3 spawnPos;
        private Direction spawnGravityDirection;
        private bool isRollContinue;

        public override Direction GravityDirection
        {
            set
            {
                base.GravityDirection = value;
                if (!respawned)
                {
                    orientation.SetDownDirection(value);
                }
            }
        }
        public override uint UpdatePriority { get; } = UpdatePriorityList.Q;
        public override bool IsOnGround
        {
            get
            {
                //TODO: отрефачить и запихать в Roll
                if (CurrentState == StateType.Roll)
                {
                    Vector3 fwd = rollState.ForwardDirection.ToVector();

                    Vector3 underPos1 = rollState.RotationPoint + fwd / 2 - orientation.CurrentPose.up / 2;
                    Vector3 underPos2 = rollState.RotationPoint - fwd / 2 - orientation.CurrentPose.up / 2;

                    Vector3Int underCell1 = Vector3Int.FloorToInt(underPos1);
                    Vector3Int underCell2 = Vector3Int.FloorToInt(underPos2);

                    if (!Environment.IsCellWalkable(underCell2) || !Environment.IsCellWalkable(underCell2))
                    {
                        return true;
                    }

                    var underActors1 = Environment.GetActorsAtPosition(underCell1);
                    var underActors2 = Environment.GetActorsAtPosition(underCell2);

                    bool underCell1Busy = false;
                    bool underCell2Busy = false;

                    foreach (var actor in underActors1)
                    {
                        if (!actor.IsCanEnter(underCell1, GravityDirection))
                        {
                            underCell1Busy = true;
                        }
                    }

                    foreach (var actor in underActors2)
                    {
                        if (!actor.IsCanEnter(underCell2, GravityDirection))
                        {
                            underCell2Busy = true;
                        }
                    }

                    return underCell1Busy || underCell2Busy;
                }
                return base.IsOnGround;
            }
        }
        public override Vector3Int[] TakedCells
        {
            get
            {
                if (CurrentState == StateType.Roll)
                {
                    return rollState.TakedCells;
                }
                return base.TakedCells;
            }
        }

        public Roll RollState => rollState;
        public bool IsRollContinue
        {
            get
            {
                return StateMachine.CurrentStateType == StateType.Idle &&
                       StateMachine.OldStateType == StateType.Roll &&
                       isRollContinue;
            }
        }
        public Vector3Int StartPositionForRoll => GlobalStaticPosition;
        public IOrientation Orientation => orientation;

        [Inject]
        private void Construct(StateMachine sm, IEnvironment environment, Q_Orientation orientation, Collider collider, Q_Settings settings)
        {
            base.Construct(collider, sm, environment);

            this.orientation = orientation;
            this.settings = settings;

            stepAnalyzer = new StepAnalyzer(environment, this, settings, orientation);
            rollState = StateMachine.FindState<Roll>();
            orientation.FollowTarget = ActorT;
        }

        private void Start()
        {
            spawnGravityDirection = GravityDirection;
            spawnPos = ActorT.position;
            Environment.RegisterObjectAsLevelPart(this);
        }

        public override bool IsCanEnter(Vector3Int position, Direction fromTo)
        {
            if (CurrentState != StateType.Roll)
            {
                return base.IsCanEnter(position, fromTo);
            }

            bool isParralelToRotationAxis = rollState.RotationAxis == fromTo.ToVector() ||
                                            rollState.RotationAxis == fromTo.Flip().ToVector();
            bool isTakedCellRoll = false;
            foreach (var cell in TakedCells)
            {
                if (cell == position)
                {
                    isTakedCellRoll = true;
                    break;
                }
            }

            return !isTakedCellRoll || !isParralelToRotationAxis;
        }

        public override TickResult Tick(float simulationTime, bool resimulateAllowance)
        {
            if (CurrentState == StateType.Roll)
            {
                return HandleStepTick(simulationTime);
            }
            return base.Tick(simulationTime, resimulateAllowance);
        }
        public override void SaveState()
        {
            if (keeper == null)
            {
                keeper = new StateKeeper();
            }
            StateMachine.SaveState();
            keeper.Pose = new Pose(ActorT.position, ActorT.rotation);
        }
        public override void Discard()
        {
            StateMachine.LoadState();
            ActorT.position = keeper.Pose.position;
            ActorT.rotation = keeper.Pose.rotation;
        }

        protected override bool ChooseState()
        {
            if (base.ChooseState())
            {
                return true;
            }

            StepInfo rollInfo = stepAnalyzer.GetStepInfo(GetInput());
            if (rollInfo != null)
            {
                StateMachine.SetState(rollState, null, rollInfo.RollSettings);
                return true;
            }

            return false;
        }
        protected override TickResult HandleFallTick(float simulationTime, bool resimulateAllowance)
        {
            var result = base.HandleFallTick(simulationTime, resimulateAllowance);

            if (CurrentState == StateType.Idle)
            {
                respawned = false;
                return result;
            }

            if (!Environment.IsVisibleOnScreen && !respawned && !orientation.IsRotating)
            {
                respawned = true;
                CenterPosition = spawnPos + Vector3.up * settings.UnderSpawnHeight;
                GravityDirection = spawnGravityDirection;
                orientation.Teleport(spawnGravityDirection);
                return result;
            }

            return result;
        }

        private TickResult HandleStepTick(float simulationTime)
        {
            Roll.RotationForceType rotationForceType = Roll.RotationForceType.OnlyGravity;
            Vector2 input = GetInput();
            Vector3Int currentForward = stepAnalyzer.GetRollDirection(input);

            if (IsOnGround)
            {
                if (currentForward == rollState.ForwardDirection.ToVector())
                {
                    rotationForceType = Roll.RotationForceType.Normal;
                }
                else if (currentForward == rollState.ForwardDirection.Flip().ToVector())
                {
                    rotationForceType = Roll.RotationForceType.Inversed;
                }
            }

            Roll.TickArgs tickArgs = new Roll.TickArgs(simulationTime, rotationForceType);
            Roll.TickResult result = rollState.Tick(tickArgs) as Roll.TickResult;

            if (result.IsRollEnded)
            {
                bool canContinueRoll = stepAnalyzer.GetStepInfo(input) != null;
                isRollContinue = currentForward != Vector3.zero && canContinueRoll ? true : false;
                StateMachine.SetState(null, null, null);
                return new TickResult(simulationTime - result.TimeLeft);
            }
            else
            {
                return new TickResult(simulationTime);
            }
        }

        private Vector2 GetInput()
        {
            //return new Vector2(0, 1);
            return new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        }

        private class StateKeeper
        {
            public Pose Pose { get; set; }
        }

#if UNITY_EDITOR
        public bool DebugRespawnded => respawned;
#endif
    }
}