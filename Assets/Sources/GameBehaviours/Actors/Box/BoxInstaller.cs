using qBox.GameBehaviours.qBoxStateMachine;
using qBox.GameBehaviours.qBoxStateMachine.States;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace qBox.GameBehaviours.Actors
{
    [RequireComponent(typeof(Box))]
    public class BoxInstaller : MonoInstaller
    {
        [SerializeField] private GlobalGameSettings settings;

        public override void InstallBindings()
        {
            StateMachine sm = CreateStateMachine();
            Container.Bind<StateMachine>().FromInstance(sm).AsSingle();
            Container.Bind<Collider>().To<BoxCollider>().FromComponentInChildren().AsSingle();
        }

        private StateMachine CreateStateMachine()
        {
            var box = GetComponent<Box>();
            List<State> states = new List<State>
            {
                new Fall(settings, box),
                new Push(box, settings)
            };
            return new StateMachine(states);
        }
    }
}