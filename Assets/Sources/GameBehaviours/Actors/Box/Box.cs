﻿using qBox.GameBehaviours.qBoxStateMachine;
using qBox.GameBehaviours.qBoxStateMachine.States;
using qBox.GameBehaviours.World;
using System;
using UnityEngine;
using Zenject;

namespace qBox.GameBehaviours.Actors
{
    [SelectionBase, DisallowMultipleComponent]
    public class Box : ActorBase, IPushable
    {
        private Direction pushDirection;
        float lastPushVelocity;

        private StateKeeper keeper;
        private Push pushState;

        public override bool IsOnGround
        {
            get
            {
                if (CurrentState == StateType.Push)
                {
                    return false;
                }
                return base.IsOnGround;
            }
        }
        public override uint UpdatePriority
        {
            get
            {
                switch (CurrentState)
                {
                    case StateType.Fall: return UpdatePriorityList.BoxFall;
                    case StateType.Push: return UpdatePriorityList.BoxPush;
                    case StateType.Idle: return UpdatePriorityList.BoxIdle;
                    default: throw new Exception("Unhandled update priority for current state!");
                }
            }
        }
        public override Vector3Int[] TakedCells
        {
            get
            {
                if (CurrentState == StateType.Push)
                {
                    return pushState.TakedCells;
                }
                return base.TakedCells;
            }
        }

        [Inject]
        private void Construct(IEnvironment enviroment, Collider collider, StateMachine stateMachine)
        {
            base.Construct(collider, stateMachine, enviroment);
            keeper = new StateKeeper();
            pushState = stateMachine.FindState<Push>();
        }

        private void Start()
        {
            Environment.RegisterObjectAsLevelPart(this);
        }

        public override bool IsCanEnter(Vector3Int position, Direction fromTo)
        {
            if (CurrentState == StateType.Push)
            {
                //TODO: реализовать
            }
            return base.IsCanEnter(position, fromTo);
        }
        public bool IsCanPush(Direction fromTo)
        {
            if (CurrentState == StateType.Idle)
            {
                Vector3Int backPos = GlobalStaticPosition + fromTo.ToVector();

                if (!Environment.IsCellWalkable(backPos))
                {
                    return false;
                }

                IActor[] onPushWayObjects = Environment.GetActorsAtPosition(backPos);
                foreach (var obj in onPushWayObjects)
                {
                    if (!obj.IsCanEnter(backPos, fromTo))
                    {
                        return false;
                    }
                }

                return true;
            }

            if (CurrentState == StateType.Fall)
            {
                bool parallelDirection = FallState.FallDirection == fromTo ||
                                         FallState.FallDirection.Flip() == fromTo;
                if (parallelDirection)
                {
                    return true;
                }
            }

            return false;
        }

        public override void Discard()
        {
            ActorT.position = keeper.Pose.position;
            ActorT.rotation = keeper.Pose.rotation;
            StateMachine.LoadState();
        }
        public override void SaveState()
        {
            StateMachine.SaveState();
            keeper.Pose = new Pose(ActorT.position, ActorT.rotation);
        }

        public override TickResult Tick(float simTime, bool resimulateAllowance)
        {
            if (CurrentState == StateType.Push)
            {
                return HandlePushTick(simTime);
            }
            return base.Tick(simTime, resimulateAllowance);
        }
        protected override bool ChooseState()
        {
            if (CurrentState != StateType.Idle)
            {
                return false;
            }

            bool changed = base.ChooseState();
            if (changed)
            {
                FallState.VelocityMagnitude = lastPushVelocity;
                return true;
            }

            IActor[] actorInBoxCell = Environment.GetActorsAtPosition(GlobalStaticPosition,
                                      (x) => { return x is IPusher; }, this);

            //TODO: Обработка больше двух толкающих
            if (actorInBoxCell.Length > 0)
            {
                var pusher = actorInBoxCell[0] as IPusher;
                CollisionResolverCalcInfo info = new CollisionResolverCalcInfo(pusher, this);
                var result = CollisionResolver.CalcCollision(info);
                if (result != null)
                {
                    SetPushState(pusher);
                    return true;
                }
            }
            return false;
        }
        private void SetPushState(IPusher pusher)
        {
            if (CurrentState == StateType.Idle)
            {
                StateMachine.SetState(pushState, null, new Push.EnableSettings()
                {
                    PushDirection = pushDirection,
                    Pusher = pusher
                });
            }
        }
        public void SetPushDirection(Direction fromTo)
        {
            pushDirection = fromTo;
        }

        private TickResult HandlePushTick(float simTime)
        {
            var result = pushState.Tick(new Push.TickArgs(simTime)) as Push.TickResult;
            if (result.IsPushEnded)
            {
                StateMachine.SetState(null, null, null);
                lastPushVelocity = result.Velocity;
                ChooseState();
            }

            return new TickResult(simTime - result.TimeLeft);
        }

        private class StateKeeper
        {
            public Pose Pose { get; set; }
        }
    }
}