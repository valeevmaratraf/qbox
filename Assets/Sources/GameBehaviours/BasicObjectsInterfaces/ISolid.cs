﻿using UnityEngine;

namespace qBox.GameBehaviours
{
    public interface ISolid : IQBoxObject
    {
        Collider Collider { get; }
        Quaternion Rotation { get; }
        Vector3Int Size { get; }
        Vector3Int[] TakedCells { get; }
    }
}