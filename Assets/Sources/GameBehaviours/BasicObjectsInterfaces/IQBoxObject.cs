﻿using System;

using UnityEngine;

namespace qBox.GameBehaviours
{
    public interface IQBoxObject
    {
        Vector3Int GlobalStaticPosition { get; }
        Vector3 CenterPosition { get; set; }

        event Action<IQBoxObject> OnDestroyEvent;
        event Action<IQBoxObject, bool> OnEnableChange;
    }
}