﻿using qBox.GameBehaviours.World;
using UnityEngine;

namespace qBox.GameBehaviours
{
    public interface IVisibleEnviromentStaticPart
    {
        Renderer[] Parts { get; }
        IEnvironment Environment { get; }
    }
}