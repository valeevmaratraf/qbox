using Zenject;
using NUnit.Framework;
using UnityEngine.TestTools;
using System.Collections;
using qBox.GameBehaviours.World;
using qBox.GameBehaviours;
using UnityEngine;

namespace qBox.Tests
{
	[TestFixture]
	public class LevelUnitTest : ZenjectUnitTestFixture
	{
		private Level level;

		[Test]
		public void LevelCreateTest()
		{
			GameObject root = CreateUpdateManager();
			CreateLevel(root);
		}

		private GameObject CreateUpdateManager()
		{
			var root = new GameObject();
			Container.Bind<UpdateManager>().FromNewComponentOn(root).AsSingle();
			return root;
		}

		private void CreateLevel(GameObject root)
		{
			//Level.ConstructArgs args = new Level.ConstructArgs()
			//{
			//	Objects = new IQBoxObject[0]
			//};
			//Container.Bind<Level.ConstructArgs>().FromInstance(args).AsCached();

			var levelGO = new GameObject();
			levelGO.transform.parent = root.transform;

			level = Container.InstantiateComponent<Level>(levelGO);
		}

		private void CreateSomeBlocks()
		{
			var blockGO = new GameObject();
			blockGO.transform.parent = level.transform;
			Container.Bind<Block>().FromNewComponentOn(blockGO).AsCached();
		}

		[UnityTest]
		public IEnumerator LevelGettingObjectsTest()
		{
			var root = CreateUpdateManager();
			CreateLevel(root);
			CreateSomeBlocks();
			//Assert.IsTrue(level.GetAllObjects().Length > 0);

			yield return new WaitForEndOfFrame();
		}
	}
}