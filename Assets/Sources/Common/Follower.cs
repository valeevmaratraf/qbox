using UnityEngine;

namespace qBox
{
    //TODO: ����������� Update ����� UpdateManager
    [ExecuteInEditMode, DisallowMultipleComponent]
    public class Follower : MonoBehaviour
    {
        [SerializeField] private Transform followingTarget = null;

        Transform target;

        public Transform FollowTarget
        {
            get { return followingTarget; }
            set { followingTarget = value; }
        }

        private void Awake()
        {
            target = transform;
        }

        private void Update()
        {
            if (!Application.isPlaying)
            {
                Move();
            }
        }

        public void Move()
        {
            if (FollowTarget)
            {
                target.position = FollowTarget.position;
            }
        }
    }
}