﻿using System;
using UnityEngine;

namespace qBox
{
    public static class Utilites
    {
        /// <summary>
        /// Round float value to nearest approximation with added offset.
        /// </summary>
        /// <param name="value">Value which will be rounded.</param>
        /// <param name="approximation">Sampling rate for rounding.</param>
        /// <param name="offset">Offset for result.</param>
        /// <returns>Returns rounded float value with added offset.</returns>
        public static float RoundTo(this float value, float approximation, float offset = 0)
        {
            if (approximation <= 0)
                throw new ArgumentException(string.Format("Could not round the value, because the approximation ({0}) " +
                                             "is less than or equal to zero!", approximation));
            value -= offset;
            value /= approximation;
            value = Mathf.Round(value);
            return value * approximation + offset;
        }
        public static Vector3 RoundTo(this Vector3 value, Vector3 approximation, Vector3 offset = default)
        {
            value.x = value.x.RoundTo(approximation.x, offset.x);
            value.y = value.y.RoundTo(approximation.y, offset.y);
            value.z = value.z.RoundTo(approximation.z, offset.z);
            return value;
        }
        public static Vector3 ClampMin(this Vector3 value, Vector3 min)
        {
            if (value.x < min.x)
                value.x = min.x;
            if (value.y < min.y)
                value.y = min.y;
            if (value.z < min.z)
                value.z = min.z;
            return value;
        }
        /// <summary>
        /// Convert direction form enum value to Vector3Int value.
        /// </summary>
        /// <param name="direction">Direction which need convert to Vector3Int.</param>
        /// <returns>Returns converted to Vector3Int direction.</returns>
        public static Vector3Int ToVector(this Direction direction)
        {
            switch (direction)
            {
                case Direction.Back: return new Vector3Int(0, 0, -1);
                case Direction.Forward: return new Vector3Int(0, 0, 1);
                case Direction.Down: return Vector3Int.down;
                case Direction.Up: return Vector3Int.up;
                case Direction.Right: return Vector3Int.right;
                case Direction.Left: return Vector3Int.left;
            }
            throw new ArgumentException(string.Format("Wrong direction enum ({0}) for convert! " +
                                        "Method: ToVector.", (int)direction));
        }
        /// <summary>
        /// Convert direction form Vector3Int value to enum value.
        /// </summary>
        /// <param name="direction">Direction which need convert to enum.</param>
        /// <returns>Returns converted to enum direction.</returns>
        public static Direction ToEnum(this Vector3Int direction)
        {
            Vector3Int forward = new Vector3Int(0, 0, 1);

            if (direction == forward) return Direction.Forward;
            else if (direction == forward * (-1)) return Direction.Back;
            else if (direction == Vector3Int.down) return Direction.Down;
            else if (direction == Vector3Int.up) return Direction.Up;
            else if (direction == Vector3Int.right) return Direction.Right;
            else if (direction == Vector3Int.left) return Direction.Left;
            else throw new ArgumentException(string.Format("Wrong direction vector ({0}) for convert! " +
                                        "Method: ToEnum", direction));
        }
        /// <summary>
        /// Flip enum direction.
        /// </summary>
        /// <param name="direction">Direction which need flip.</param>
        /// <returns>Returns flipped direction.</returns>
        public static Direction Flip(this Direction direction)
        {
            switch (direction)
            {
                case Direction.Back: return Direction.Forward;
                case Direction.Forward: return Direction.Back;
                case Direction.Right: return Direction.Left;
                case Direction.Left: return Direction.Right;
                case Direction.Up: return Direction.Down;
                case Direction.Down: return Direction.Up;
            }
            throw new ArgumentException(string.Format("Wrong direction vector ({0}) for convert! " +
                                        "Method: Flip.", direction));
        }

        public static Vector3Int[] GetNearPositions(this Vector3Int cell)
        {
            Vector3Int back = cell + new Vector3Int(0, 0, -1);
            Vector3Int forward = cell + new Vector3Int(0, 0, 1);
            Vector3Int left = cell - Vector3Int.right;
            Vector3Int right = cell + Vector3Int.right;
            Vector3Int up = cell + Vector3Int.up;
            Vector3Int down = cell - Vector3Int.up;
            return new Vector3Int[] { back, forward, left, right, up, down, cell };
        }

        public static float ExtractComponent(this Vector3 target, Axis axis)
        {
            switch (axis)
            {
                case Axis.X: return target.x;
                case Axis.Y: return target.y;
                case Axis.Z: return target.z;
            }

            throw new ArgumentException(string.Format("Wrong axis ({0}) for extract component of vector! " +
                                        "Method: ExtractComponent.", axis));
        }

        public static Axis GetAxis(this Direction direction)
        {
            switch (direction)
            {
                case Direction.Forward: case Direction.Back: return Axis.Z;
                case Direction.Up: case Direction.Down: return Axis.Y;
                case Direction.Left: case Direction.Right: return Axis.X;
            }
            throw new ArgumentException(string.Format("Wrong direction ({0}) for get axis of direction! " +
                                    "Method: GetAxis.", direction));
        }

        public static int GetSign(this Direction direction)
        {
            switch (direction)
            {
                case Direction.Back: case Direction.Left: case Direction.Down: return -1;
                case Direction.Forward: case Direction.Right: case Direction.Up: return 1;
            }
            throw new ArgumentException(string.Format("Wrong direction ({0}) for get direction sign! " +
                                "Method: GetSign.", direction));
        }
        //public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
        //{
        //	return listToClone.Select(item => (T)item.Clone()).ToList();
        //}
    }
}