namespace qBox
{
    /// <summary>
    /// More readable representation of direction in 3D space.
    /// </summary>
    public enum Direction { Up, Down, Right, Left, Forward, Back }

    /// <summary>
    /// More readable representation of direction in 2D space.
    /// </summary>
    public enum Durection2D { Top, Bottom, Right, Left }

    /// <summary>
    /// Represents clockwise or anticlockwise rotate values.
    /// </summary>
    public enum RotateDirection { Clockwise = 1, Anticlockwise = -1 }

    public enum Axis { X, Y, Z }
}